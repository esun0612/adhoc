#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 19 15:20:05 2019

@author: esun
"""
from util_analysis import gen_plot_conv_data
data_file = '../round_2/data_lr/PL_rate_oot_2.csv'
rate_model_file = '../round_2/experiments/7c_2019-11-22_19-48-31/model_rate_7c.txt'
model_header_file = '../round_2/experiments/7c_2019-11-22_19-48-31/round2_7c_rate_header.csv'

df, rate_chgs = gen_plot_conv_data(data_file, rate_model_file, model_header_file)
df.to_csv('../adhoc/plot_conv/conv_sensitivity_data.csv', index=None)

plot_conv_sensitivity_wrap(df, rate_chgs, '../adhoc/plot_conv/conv_sensitivity.pdf')
