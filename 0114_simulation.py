#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 16:50:37 2020

@author: esun
"""

from os.path import join as pjoin
import numpy as np
import pandas as pd
import sys
import glob
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/pl_model')
from util_model import score_model, load_json, score_term_model, Header
from util_data import add_signed_ind, clean_str, esave, eread, flat_term_rate_dat, flat_rate, query_sofidw, query_snowflake, impute_rate, divide_train_oos 
from util_data import calc_monthly_payments, calc_payments, check_trans_to_list, change_cols
from util_data import read_grid, create_fico_loan_bin, merge_grid_price
from header_to_model import merge_all_pred, report_performance_metrics
from util_model import score_model, merge_rate_model

start_date = '2019-12-01'
end_date = '2020-01-13'
product = f"'PL'"
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"

df = query_snowflake(PL_qry)
df.shape
#(38583, 70)
dat_dir = '../adhoc/0114_louis_simulation'
esave(df, pjoin(dat_dir, '0114_louis_raw_data.feather'))

df_rate = query_sofidw(rate_qry)
print(df_rate.shape, df_rate['id'].nunique())
#(521681, 6) 39057
esave(df_rate, pjoin(dat_dir, 'raw_rate.feather'))

rate_qry = f"""SELECT
af.id,
af.requested_amount,
p.product_term,
of.min_rate,
SUBSTRING(o.min_tier_code,'[1-9]')::INT AS tier,
of.max_amount
FROM dwmart.applications_file af
left JOIN product_application_facts paf on af.dw_application_id = paf.application_id
JOIN underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf. initial_uw_id, 0))
JOIN offer_facts of ON of.underwriting_info_id = ui.underwriting_info_id
JOIN offer_details o on of.offer_details_id = o.offer_details_id
JOIN products p ON p.product_id = of.product_id
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
and af.application_type = {product} AND {date_range}
AND af.current_decision = 'ACCEPT'
"""

PL_qry = f"""
SELECT
af.id,
af.date_signed,
af.date_doc_upload,
af.date_start,
af.initial_term,
af.interest_rate,
af.requested_amount,
af.gross_income,
af.app_created_via_mobile,
af.g_program,
af.g_grad_year,
af.credit_score,
af.employer_name,
af.years_of_experience,
af.pl_funds_use,
af.tax_burden_amount,
af.consolidated_channel,
af.attr_affiliate_referrer,
af.campaign_welcome_bonus,
af.housing_status,
acaf.pil0438,
af.registration_date,
af.revolving_credit_amount,
af.tier,
af.ug_ctgry,
af.ug_grad_year,
af.ug_program,
af.member_indicator,
acaf.iln5020,
acaf.alx8220,
acaf.all8020,
acaf.iln5820,
acaf.all0416,
acaf.REV0416,
acaf.mtf5020,
acaf.mta5020,
acaf.mta5830,
acaf.mtj5030,
acaf.mtj5820,
acaf.all5820,
acaf.all7516,
acaf.all8220,
acaf.all5020,
acaf.bcc7110,
acaf.bcc8322,
acaf.bcx3421,
acaf.bcx3422,
acaf.bcx5020,
acaf.bcx5320,
acaf.bcx7110,
acaf.rev5020,
acaf.rev8320,
acaf.rta7300,
acaf.rtr5030,
acaf.all7517,
acaf.iln0416,
acaf.mta0416,
acaf.mta1370,
acaf.mta2800,
acaf.mta8150,
acaf.mta8153,
acaf.mta8157,
acaf.mta8160,
acaf.mtf0416,
acaf.mtf4260,
acaf.mtf8166,
acaf.mts5020,
af.free_cash_flow_pre,
CASE WHEN af.date_doc_upload IS NOT NULL THEN 1 ELSE 0 END AS docs_ind,
CASE WHEN af.date_signed IS NOT NULL THEN 1 ELSE 0 END AS signed_ind
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL)
AND af.current_decision = 'ACCEPT'
"""

#2. Clean rate data
df_rate = eread(pjoin(dat_dir, 'raw_rate.feather'))
df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df4 = df2[df2['max_amount'] >= df2['requested_amount']]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier'], consolidate_cols=[])
df4_new.shape
#(35550, 12)
df4_new.to_csv(pjoin(dat_dir, 'entire_rate.csv'))

#3. Clean main data
df = eread(pjoin(dat_dir, '0114_louis_raw_data.feather'))
df = pd.merge(df, df4_new[[f'tier_{term}' for term in range(2,8)]], on='id')
df.shape
#(35103, 76)
esave(df, pjoin(dat_dir, 'entire_rate_tier.feather'))

#4. Read grid
df = eread(pjoin(dat_dir, 'entire_rate_tier.feather'))
grid = read_grid(pjoin(dat_dir, 'at_grid.xlsx'), rate_col_nm='min_rate')
df = create_fico_loan_bin(df)
df = merge_grid_price(df, grid, terms=range(2,8), rate_col_nm='min_rate')
esave(df, pjoin(dat_dir, 'at.feather'))

#5. process data
impute_rate_dict={'min_rate_2': 0.16677,
                 'min_rate_3': 0.16677,
                 'min_rate_4': 0.17436,
                 'min_rate_5': 0.18133,
                 'min_rate_6': 0.15313,
                 'min_rate_7': 0.16115}

def process(df, impute_rate_dict):
    #Member indicator, grad to 0/1 variable
    df['grad'] = np.where(df['g_program'].notnull(), 1, 0)
    #Define date related variables
    df['date_start'] = pd.to_datetime(df['date_start'])
    min_start_date = df['date_start'].min()
    df['date_start_year'] =  df['date_start'].dt.year
    df['date_start_month'] = df['date_start'].dt.month
    df['date_start_day'] = df['date_start'].dt.day
   
    df['attr_affiliate_referrer'] = clean_str(df['attr_affiliate_referrer'])
    g_program_list = ['Business - MBA', 'Other', 'Law - JD', 'Engineering/Computer Science - MS or higher',
                                  'Business - MS or MA or higher', 'Social Sciences - MS or MA or higher']
    channel_list = ['www.sofi.com', 'Affiliate', 'SEM', 'Organic Search', 'MKT-DM', 'P2P']
    affiliate_list = ['www.sofi.com', 'www.google.com', '2019_pl_sofi_mail', 'credit_karma', 'lending_tree', 'nerdwallet']
    df = cut_cat_var(df, 'g_program', g_program_list)
    df = cut_cat_var(df, 'consolidated_channel', channel_list)
    df = cut_cat_var(df, 'attr_affiliate_referrer', affiliate_list)
    
    #Create indicator variables for selected each term or not
    PL_terms = [2, 3, 4, 5, 6, 7]
    for term in PL_terms:
        df[f'initial_term_{term}'] = np.where((df['initial_term'] == term), 1, 0)
    #Map interest_rate to individual term columns
    df = flat_rate(df, PL_terms)
    #Consolidate tier columns
    df['tier_pre'] = df[[f'tier_{term}' for term in PL_terms]].min(axis=1)

    df['g_grad_year'] = df['date_start'].dt.year - df['g_grad_year']
    df['ug_grad_year'] = df['date_start'].dt.year - df['ug_grad_year']
    #registration_date: date_start - var
    df['registration_date'] = pd.to_datetime(df['registration_date'])
    df['registration_date'] = (df['date_start'] - df['registration_date']).dt.days
    
    df.rename(columns={f'rate_{term}': f'min_rate_{term}' for term in PL_terms}, inplace=True)
    df, _ = impute_rate(df, rate_cols=[f'min_rate_{term}' for term in PL_terms], impute_rate_dict=impute_rate_dict)
    rate_cols = [f'min_rate_{term}' for term in range(2,8)]
    df['min_all_rates'] = df[rate_cols].min(axis=1) #Minimum of all rates
    
    for term in PL_terms:
        df[f'monthly_payment_{term}'] = calc_monthly_payments(df['requested_amount'], term, df[f'min_rate_{term}_orig'])
        df[f'free_cash_flow_post_{term}'] = df['free_cash_flow_pre'] - df[f'monthly_payment_{term}']
    monthly_payment_cols = [f'monthly_payment_{term}' for term in PL_terms]
    df['min_monthly_payment'] = df[monthly_payment_cols].min(axis=1)
    df['min_monthly_payment'].fillna(df['min_monthly_payment'].max(), inplace=True)
    df['free_cash_flow_post_max'] = df['free_cash_flow_pre'] - df['min_monthly_payment']
    
    for term in range(2, 7):
        df[f'min_rate_diff_{term+1}_{term}'] = df[f'min_rate_{term+1}'] - df[f'min_rate_{term}']
    return df

df2 = process(df, impute_rate_dict)
#df2.describe().transpose().to_csv(pjoin(dat_dir, 'AS_stat.csv'))
PL_terms = [2, 3, 4, 5, 6, 7]
df2 = add_signed_ind(df2, terms=PL_terms, remove_mix_labels=False, filterdays=365)
df2.to_csv(pjoin(dat_dir, 'at_pp.csv'), index=None)
df2.shape
#(35103, 133)

model_dir = '/home/ec2-user/SageMaker/round_2/experiments/7g_2019-11-26_16-29-29'
out_dir = pjoin(dat_dir, 'model_at')
#Score rate
raw_rate = score_model(glob.glob(pjoin(model_dir, 'model_rate*.txt'))[0], pjoin(out_dir, 'rate_pred_raw.csv'), 
            data_file=pjoin(dat_dir, 'at_pp.csv'), header_file=glob.glob(pjoin(model_dir, '*_rate_header.csv'))[0])
pred_rate = merge_rate_model(pjoin(dat_dir, 'at_pp.csv'), pjoin(out_dir, 'rate_pred_raw.csv'), pjoin(out_dir, 'rate_pred.csv'))

#Score term
score_model(glob.glob(pjoin(model_dir, 'model_term*.txt'))[0], pjoin(out_dir, 'term_pred.csv'), 
            data_file=pjoin(dat_dir, 'at_pp.csv'), header_file=glob.glob(pjoin(model_dir, '*_term_header.csv'))[0])

params_rate = load_json(glob.glob(pjoin(model_dir, 'params_rate_*.json'))[0]) 
terms = params_rate['terms']
rate_cols = ['{0}_{1}'.format(params_rate['term_col_prefix'], term) for term in terms]
pred_term_col = 'term_wgt_prob'
pred_term_prob_cols = [col + '_prob_adj' for col in rate_cols]
rate_orig_cols = [col + '_orig' for col in rate_cols]
target_term_col = 'initial_term'
target_term_prob_cols = [f'{target_term_col}_{term}' for term in terms]

term_score = score_term_model(data_file=pjoin(dat_dir, 'at_pp.csv'), raw_model_output_file=pjoin(out_dir, 'term_pred.csv'), 
                 output_file=pjoin(out_dir, 'pred_term_final.feather'), 
                 impute_rate_cols=rate_cols, orig_rate_cols=rate_orig_cols, terms=terms, target_term_prob_cols=target_term_prob_cols)

#Evaluation
act_term_prob_cols = ['signed_ind_{}'.format(term) for term in terms]  
rate_map_cols = [f'rate_map_{term}' for term in terms]
df_final = merge_all_pred(pjoin(out_dir, 'rate_pred.csv'), pjoin(out_dir, 'pred_term_final.feather'), 
                          pjoin(dat_dir, 'at_pp.csv'), act_term_prob_cols, rate_map_cols, output_file=pjoin(out_dir, 'pred_final.feather'))

final_result = report_performance_metrics([df_final], ['oot'], terms, act_term_prob_cols,
                                          pred_term_prob_cols, rate_map_cols, rate_cols, 'tier', 'tier_pre')
final_result.to_csv(pjoin(out_dir, "final_metrics_at.csv"))


