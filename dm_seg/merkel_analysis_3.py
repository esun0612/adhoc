#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 09:26:16 2020

@author: esun
"""
import numpy as np
import pandas as pd
import re
import sys
sys.path.append('../erika_git/erika_utils')
sys.path.append('../erika_git/optimization/opt_utils')
from util_data import eread
from opt_metrics import basic_metrics
from opt_util_data import create_fico_loan_income_bin

df_3 = eread('../adhoc/dm_seg/dm_seg_data_process.csv')
df_score = pd.read_csv('../adhoc/dm_seg/residual_score.csv')
df_4 = pd.merge(df_3, df_score, on='id')

#Original score
rate_coef = pd.read_csv('../pl_opt/opt_0330/model/elasticity_coef_table_0330.csv')
rate_pred = rate_score(df_4, rate_coef, [f'min_rate_{term}' for term in range(2, 8)])
term_coef = pd.read_csv('../pl_opt/opt_0330/model/Term_Selection_Coef_0330.csv')
term_coef.set_index('Variable', inplace=True) 
term_pred = term_score(df_4, term_coef, [f'min_rate_{term}' for term in range(2, 8)])
final_score = pd.merge(rate_pred, term_pred, on='id')
final_score = cal_avg(final_score, [f'min_rate_{term}' for term in range(2, 8)])
basic_metrics(final_score, 'rate_pred', 'avg_rate')
#(0.2407580846083371, 0.24782009456900134, 0.12042537756277542)

#Loop for change on wac
def change_rate(df, chg_delta):
    for term in range(2, 8):
        conditions = [df['merkle_cluster'] == 1, df['merkle_cluster'] == 3, 
                      df['merkle_cluster'] == 4,
                      df['merkle_cluster'] == 25, df['merkle_cluster'] == 6]
        choices = [df[f'min_rate_{term}'] + chg_delta[0], df[f'min_rate_{term}'] + chg_delta[1], 
                   df[f'min_rate_{term}'] + chg_delta[2],
                   df[f'min_rate_{term}'] + chg_delta[3], df[f'min_rate_{term}'] + chg_delta[4]]
        df[f'min_rate_{term}_orig'] = df[f'min_rate_{term}']
        df[f'min_rate_{term}'] = np.select(conditions, choices)
    return df

def score(df, rate_coef, term_coef, rate_chgs):
    df_new = change_rate(df.copy(), rate_chgs)
    rate_pred = rate_score(df_new, rate_coef, [f'min_rate_{term}' for term in range(2, 8)])
    term_pred = term_score(df_new, term_coef, [f'min_rate_{term}' for term in range(2, 8)])
    final_score = pd.merge(rate_pred, term_pred, on='id')
    final_score = cal_avg(final_score, [f'min_rate_{term}' for term in range(2, 8)])
    return final_score


delta_1 = np.arange(-0.015, -0.0095, 0.0005)
delta_2 = np.arange(-0.010, -0.0045, 0.0005)
delta_3 = np.arange(-0.005, -0.0025, 0.0005)
delta_4 = np.arange(0.001, 0.0035, 0.0005)
delta_5 = np.arange(0.003, 0.0075, 0.0005)

result = []
for d1 in delta_1:
    #lower bound
    [d2, d3, d4, d5] = [delta_2[0], delta_3[0], delta_4[0], delta_5[0]]
    final_score = score(df_4, rate_coef, term_coef, [d1, d2, d3, d4, d5])
    wac = basic_metrics(final_score, 'rate_pred', 'avg_rate')[-1]
    print(d1, 'lower', wac)
    if wac - 0.1204 > 0.001:
        continue
    [d2, d3, d4, d5] = [delta_2[-1], delta_3[-1], delta_4[-1], delta_5[-1]]
    final_score = score(df_4, rate_coef, term_coef, [d1, d2, d3, d4, d5])
    wac = basic_metrics(final_score, 'rate_pred', 'avg_rate')[-1]
    print(d1, 'higher', wac)
    if wac - 0.1204 < -0.001:
        continue
    for d2 in delta_2:
        for d3 in delta_3:
            for d4 in delta_4:
                for d5 in delta_5:
                    final_score = score(df_4, rate_coef, term_coef, [d1, d2, d3, d4, d5])
                    result.append([d1, d2, d3, d4, d5] + list(basic_metrics(final_score, 'rate_pred', 'avg_rate')))
                    
df = pd.DataFrame(result)
df.to_csv('../adhoc/dm_seg/result.csv', index=None)

df = pd.read_csv('../adhoc/dm_seg/result.csv')
df.columns = ['d1', 'd2', 'd3', 'd4', 'd5', 'conv', 'conv_dollar', 'wac']
df2 = df[np.abs(df['wac'] - 0.1204) <= 0.001]
df2.loc[df2['conv_dollar'].idxmax()]
df2.to_csv('../adhoc/dm_seg/result_clean.csv', index=None)
        
        
        
        
        
        
        
        
        
        