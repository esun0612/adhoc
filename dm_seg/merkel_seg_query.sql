select 
 p.kb_iid, 
   p.borrower_id, 
   af.dw_application_id,
   p.campaign_type,
   af.consolidated_channel,
   af.actual_mail_drop_date,
   af.app_created_date,
   af.initial_decision,
   merkle_cluster,
   af.application_type
from  
 (select 
        br.kb_iid,
        br.borrower_id, 
        promo.campaign_type,
        promo.actual_mail_drop_date,
        cast(sofi_channel_val_4 as bigint) as merkle_cluster -- merkle cluster
  from 
  "staging_prospectdb"."promotion_history" promo
  inner join prospectdb.m1_borrower_keys br 
   on promo.indiv_key = br.kb_iid
  where promo.prod_cell_num like 'PL%'
  and campaign_type ='PS'  -- Pre screen campaign
  and sofi_channel_val_4 is not NULL) p
 join 
  mdp_staging.dm_matchback_daily af
  on af.borrower_id = p.borrower_id
  and af.actual_mail_drop_date = p.actual_mail_drop_date
  and af.application_type = 'PL'
  
  --2017-now