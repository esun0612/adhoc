#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 09:32:42 2020

@author: esun
"""
import numpy as np
from os.path import join as pjoin
import mdsutils
import pandas as pd
import sys
sys.path.append('../erika_git/erika_utils')
from util_data import eread

#1. query seg data
ath = mdsutils.AthenaClient()    
df = ath.query_to_df(query)
df.to_csv('../adhoc/dm_seg/seg_data.csv', index=None)
#dedup
df.sort_values(by=['dw_application_id', 'actual_mail_drop_date'], inplace=True)
df = df.drop_duplicates(subset=['dw_application_id'], keep='last')
df.to_csv('../adhoc/dm_seg/seg_data_dedup.csv', index=None)

#2. merge
df_app = pd.read_csv('../adhoc/dm_seg/dm_app_data.csv')
df_2 = pd.merge(df[['dw_application_id', 'merkle_cluster']], df_app, on='dw_application_id')
print(df.shape, df_app.shape, df_2.shape)
#(135552, 10) (508337, 71) (76198, 72)
df_2.to_csv('../adhoc/dm_seg/dm_seg_data.csv', index=None)

df_3 = pd.merge(df_2, df4_new, on='id')
#df_3.shape
#Out[62]: (68082, 84)

df_3.to_csv('../adhoc/dm_seg/dm_seg_data_rate.csv', index=None)

#3. plot
#a. data process
PL_terms = [2, 3, 4, 5, 6, 7]
df_3 = add_signed_ind(df_3, terms=PL_terms)
#df_3.shape
#Out[62]: (67375, 90)
df_3 = df_3[df_3['consolidated_channel'] == 'MKT-DM']
df_3.shape
Out[88]: (26862, 90)
df_3 = process_1(df_3)
df_3['combined_organic'] = 0
df_3['Rest'] = 0
df_3 = process_2(df_3)
df_3['merkle_cluster'].value_counts()
"""
6     10701
4      6443
25     6245
3      2039
1      1434
"""
df_3.to_csv('../adhoc/dm_seg/dm_seg_data_process.csv', index=None)

#b. residual model
model_dir = '../pl_opt/opt_0330/model'

def score_rate_residual(lightgbm_model_file, data_file, header_file, output_file):
    """Score residual model 
    """
    df_orig = pd.read_csv(data_file)
    residual_pred = score_model(lightgbm_model_file, data_file=data_file, header_file=header_file)
    df = pd.DataFrame({'id': df_orig['id'],
                       'residual_pred': residual_pred[0]})
    df.to_csv(output_file, index=None)
    
score_rate_residual(lightgbm_model_file=pjoin(model_dir, 'residual_elasticity_model.txt'), 
                    data_file='../adhoc/dm_seg/dm_seg_data_process.csv', 
                    header_file='../erika_git/optimization/residual_model/header_residual_rate_3vars.csv', 
                    output_file='../adhoc/dm_seg/residual_score.csv')

#c. generate plot data
df_score = pd.read_csv('../adhoc/dm_seg/residual_score.csv')
df_4 = pd.merge(df_3, df_score, on='id')
print(df_4.shape, df_3.shape, df_score.shape)
#(26862, 196) (26862, 195) (26862, 2)
df_4, rate_chgs = gen_plot_conv_data_residual(lr_model_file='../pl_opt/opt_0330/model/elasticity_coef_table_0330.csv', df=df_4)

def plot_conv_sensitivity_wrap(df, rate_chgs, output_file, dimensions):
    """Plot conversion sensitivity
    """
    pdf = matplotlib.backends.backend_pdf.PdfPages(output_file)
    df = create_fico_loan_bin(df)
    for dimension in dimensions:
        fig = plot_conv_sensitivity(df, dimension, rate_chgs)
        pdf.savefig(fig)
        plt.close()
    pdf.close() 

plot_conv_sensitivity_wrap(df_4, rate_chgs,   '../adhoc/dm_seg/elasticity_plot.pdf', ['merkle_cluster'])
                                                                            
df_5, rate_chgs = gen_plot_conv_data_residual(lr_model_file='../pl_opt/opt_0330/model/elasticity_coef_table_0330.csv', df=df_4, 
                                            rate_chgs=[-0.015, 0.015])

def plot_slope_wrap(df, rate_chgs, output_file, dimensions, dimension_values):
    """Plot conversion slope
    """
    pdf = matplotlib.backends.backend_pdf.PdfPages(output_file)
    for dimension, dimension_value in zip(dimensions, dimension_values):
        slope, dimension_values = calculate_slope(df, dimension, rate_chgs, dimension_value)
        fig = plot_slope(slope, dimension_values, dimension)
        pdf.savefig(fig)
        plt.close()
    pdf.close() 
    
plot_slope_wrap(df_5, rate_chgs, '../adhoc/dm_seg/slope_plot.pdf', ['merkle_cluster'], [None])

#2. query dw_application_id
start_date = '2019-01-01'
end_date = '2020-03-30'
product = f"'PL'"
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"

#dm seg qury
query = """select 
     p.kb_iid, 
       p.borrower_id, 
       af.dw_application_id,
       p.campaign_type,
       af.consolidated_channel,
       af.actual_mail_drop_date,
       af.app_created_date,
       af.initial_decision,
       merkle_cluster,
       af.application_type
    from  
     (select 
            br.kb_iid,
            br.borrower_id, 
            promo.campaign_type,
            promo.actual_mail_drop_date,
            cast(sofi_channel_val_4 as bigint) as merkle_cluster -- merkle cluster
      from 
      "staging_prospectdb"."promotion_history" promo
      inner join prospectdb.m1_borrower_keys br 
       on promo.indiv_key = br.kb_iid
      where promo.prod_cell_num like 'PL%'
      and campaign_type ='PS'  -- Pre screen campaign
      and sofi_channel_val_4 is not NULL) p
     join 
      mdp_staging.dm_matchback_daily af
      on af.borrower_id = p.borrower_id
      and af.actual_mail_drop_date = p.actual_mail_drop_date
      and af.application_type = 'PL'
      """

#app data query     
PL_qry = f"""
SELECT
af.id,
af.dw_application_id, 
af.date_signed,
af.date_doc_upload,
af.date_start,
af.initial_term,
af.interest_rate,
af.requested_amount,
af.gross_income,
af.app_created_via_mobile,
af.g_program,
af.g_grad_year,
af.credit_score,
af.employer_name,
af.years_of_experience,
af.pl_funds_use,
af.tax_burden_amount,
af.consolidated_channel,
af.attr_affiliate_referrer,
af.campaign_welcome_bonus,
af.housing_status,
acaf.pil0438,
af.registration_date,
af.revolving_credit_amount,
af.tier,
af.ug_ctgry,
af.ug_grad_year,
af.ug_program,
af.member_indicator,
acaf.iln5020,
acaf.alx8220,
acaf.all8020,
acaf.iln5820,
acaf.all0416,
acaf.REV0416,
acaf.mtf5020,
acaf.mta5020,
acaf.mta5830,
acaf.mtj5030,
acaf.mtj5820,
acaf.all5820,
acaf.all7516,
acaf.all8220,
acaf.all5020,
acaf.bcc7110,
acaf.bcc8322,
acaf.bcx3421,
acaf.bcx3422,
acaf.bcx5020,
acaf.bcx5320,
acaf.bcx7110,
acaf.rev5020,
acaf.rev8320,
acaf.rta7300,
acaf.rtr5030,
acaf.all7517,
acaf.iln0416,
acaf.mta0416,
acaf.mta1370,
acaf.mta2800,
acaf.mta8150,
acaf.mta8153,
acaf.mta8157,
acaf.mta8160,
acaf.mtf0416,
acaf.mtf4260,
acaf.mtf8166,
acaf.mts5020,
af.free_cash_flow_pre,
CASE WHEN af.date_doc_upload IS NOT NULL THEN 1 ELSE 0 END AS docs_ind,
CASE WHEN af.date_signed IS NOT NULL THEN 1 ELSE 0 END AS signed_ind
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL)
AND af.current_decision = 'ACCEPT'
"""
df = query_snowflake(PL_qry)
df.to_csv('/Users/esun/Documents/erika_git/adhoc/dm_seg/dm_app_data.csv', index=None)

#rate query
rate_qry = f"""SELECT
af.id,
af.requested_amount,
p.product_term,
of.min_rate,
SUBSTRING(o.min_tier_code,'[1-9]')::INT AS tier,
of.max_amount
FROM dwmart.applications_file af
left JOIN product_application_facts paf on af.dw_application_id = paf.application_id
JOIN underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf. initial_uw_id, 0))
JOIN offer_facts of ON of.underwriting_info_id = ui.underwriting_info_id
JOIN offer_details o on of.offer_details_id = o.offer_details_id
JOIN products p ON p.product_id = of.product_id
WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
and af.application_type = {product} AND {date_range}
AND af.current_decision = 'ACCEPT' 
"""
df_rate = query_data(rate_qry, data_base='sofidw')
print(df_rate.shape, df_rate['id'].nunique())
#(6875033, 6) 514908
esave(df_rate, '../adhoc/dm_seg/rate_data.csv')

#2b. Clean rate data
df_rate.sort_values(by=['id', 'product_term', 'max_amount'], inplace=True)
df2 = df_rate.drop_duplicates(subset=['id', 'product_term', 'min_rate'], keep='last') #For each unique id, product_term and min_rate, keep the largest max_amount
df4 = df2[df2['max_amount'] >= df2['requested_amount']]
df4 = df4.sort_values(by=['id', 'product_term', 'max_amount'])
df4 = df4.drop_duplicates(subset=['id', 'product_term'], keep='first')
df4_new = flat_term_rate_dat(df4, flat_cols=['min_rate', 'tier'], consolidate_cols=[])
df4_new.shape
#(464240, 12)
esave(df4_new, '../adhoc/dm_seg/rate_data_clean.csv')