#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 12:08:06 2020

@author: esun
"""
import numpy as np
import pandas as pd
import re

#Try adjust the price see the outcome
df_3 = eread('../adhoc/dm_seg/dm_seg_data_process.csv')
df_score = pd.read_csv('../adhoc/dm_seg/residual_score.csv')
df_4 = pd.merge(df_3, df_score, on='id')

def rate_score(df_orig, rate_coef, rate_cols):
        """Score rate model
        """
        df = df_orig.copy()
        df = create_fico_loan_income_bin(df, income=False)
        cr_score_bin_name = ['less_than_700'] + [f'{lb}_{ub}' for lb, ub in zip(range(700, 800, 20), range(720, 820, 20))]
        #Define multiplication terms:
        for term, rate_col in zip(range(2, 8), rate_cols):
            for channel in ['combined_organic', 'MKT-DM', 'Rest']:
                df[f'min_rate_{term}_{channel}'] = df[rate_col] * df[f'{channel}']
            for requested_bin in ['less_than_20000', 'more_equal_50000']:
                df[f'min_rate_{term}_{requested_bin}'] = df[rate_col] * df[f'requested_amount_{requested_bin}']
            for credit_score_bin in cr_score_bin_name:
                df[f'min_rate_{term}_{credit_score_bin}'] = df[rate_col] * df[f'credit_score_{credit_score_bin}']
        #Score logistic regression
        for term, rate_col in zip(range(2, 8), rate_cols):
            df[f'pred_rate_{term}'] = 1 / (1+np.exp(-((rate_coef[f'term_{term}'].values[1:] * \
                                          df[[rate_col, 'credit_score', 'requested_amount'] +
                                         ['combined_organic', 'MKT-DM', 'Rest'] + 
                                         ['requested_amount_less_than_20000', 'requested_amount_more_equal_50000'] +
                                         [f'credit_score_{bin}' for bin in cr_score_bin_name] +
                                         [f'min_rate_{term}_{channel}' for channel in ['combined_organic', 'MKT-DM', 'Rest']] + 
                                         [f'min_rate_{term}_{requested_bin}' for requested_bin in ['less_than_20000', 'more_equal_50000']] +
                                         [f'min_rate_{term}_{credit_score_bin}' for credit_score_bin in cr_score_bin_name]].values).sum(axis=1) + 
                                         rate_coef[f'term_{term}'].values[0])))
        df['pred_from_rate'] = df[[f'pred_rate_{term}' for term in range(2, 8)]].mean(axis=1)
        df['rate_pred'] = np.clip(df['pred_from_rate'] + df['residual_pred'], 0, 1)
        return df[['id', 'rate_pred', 'tier_pre', 'requested_amount', 'credit_score', 'fico_bin', 
                   'loan_amt_bin', 'consolidated_channel_model', 'gross_income', 'merkle_cluster'] + \
                  ['combined_organic', 'MKT-DM', 'Rest'] + rate_cols]

rate_coef = pd.read_csv('../pl_opt/opt_0330/model/elasticity_coef_table_0330.csv')
rate_pred = rate_score(df_4, rate_coef, [f'min_rate_{term}' for term in range(2, 8)])
term_coef = pd.read_csv('../pl_opt/opt_0330/model/Term_Selection_Coef_0330.csv')
term_coef.set_index('Variable', inplace=True) 
term_pred = term_score(df_4, term_coef, [f'min_rate_{term}' for term in range(2, 8)])
final_score = pd.merge(rate_pred, term_pred, on='id')
final_score = cal_avg(final_score, [f'min_rate_{term}' for term in range(2, 8)])
basic_metrics(final_score, 'rate_pred', 'avg_rate')
#(0.2407580846083371, 0.24782009456900134, 0.12042537756277542)

def change_rate(df):
    for term in range(2, 8):
        conditions = [df['merkle_cluster'] == 1, df['merkle_cluster'] == 3, 
                      df['merkle_cluster'] == 4,
                      df['merkle_cluster'] == 25, df['merkle_cluster'] == 6]
        choices = [df[f'min_rate_{term}'] - 0.01, df[f'min_rate_{term}'] - 0.005, 
                   df[f'min_rate_{term}'],
                   df[f'min_rate_{term}'] + 0.005, df[f'min_rate_{term}'] + 0.01]
        df[f'min_rate_{term}_orig'] = df[f'min_rate_{term}']
        df[f'min_rate_{term}'] = np.select(conditions, choices)
    return df
df_4 = pd.merge(df_3, df_score, on='id')
df_5 = change_rate(df_4)
#qc -> pass
for merkle_segment in [1, 3, 4, 25, 6]:
    df_segment = df_5[df_5['merkle_cluster'] == merkle_segment]
    print(merkle_segment, (df_segment['min_rate_2'] - df_segment['min_rate_2_orig']).describe())

rate_pred = rate_score(df_5, rate_coef, [f'min_rate_{term}' for term in range(2, 8)])
term_pred = term_score(df_5, term_coef, [f'min_rate_{term}' for term in range(2, 8)])
final_score = pd.merge(rate_pred, term_pred, on='id')
final_score = cal_avg(final_score, [f'min_rate_{term}' for term in range(2, 8)])
basic_metrics(final_score, 'rate_pred', 'avg_rate')
#(0.23425677302065842, 0.241005333315713, 0.12584330019484266)

def cal_avg(df, rate_cols):
    """Score weighted average rate, weighted average maturity and weighted average loss
    """
    term_prob_cols = [f'pred_term_{term}' for term in range(2, 8)]
    df['avg_rate'] = np.sum(df[rate_cols].fillna(0).values * df[term_prob_cols].values, axis=1)
    return df
    
def get_segment_params(param_names, segment):
        """Load mnlogit model parameters
        """
        result = []
        for item in param_names:
            if re.search(f':{segment}', item):
                result.append(item)
        return result
    
def adjust_term_rate(df, rate_cols):
        """Adjust term prediction to have 0 on missing rate terms and sum to 1
        """
        term_prob_cols = [f'pred_term_{term}' for term in range(2, 8)]
        #Apply mask
        df = apply_mask_to_term_pred(df, rate_cols=rate_cols)
        #Adjust score to sum to 1
        df['pred_term_sum'] = df[term_prob_cols].sum(axis=1)
        for col in term_prob_cols:
            df[col] = df[col]/df['pred_term_sum']
        return df
    
def apply_mask_to_term_pred(df, rate_cols):
    """Apply mask to term prediction so that the prediction equals 0 for missing rate terms
    """
    #Change mask definition
    #Read mask from data
    term_prob_cols = [f'pred_term_{term}' for term in range(2, 8)]
    mask = ~df[rate_cols].isna() + 0
    df[term_prob_cols] = df[term_prob_cols].values * mask.values
    return df        
    
def term_score(df_orig, term_coef, rate_cols):
        """Score multinomial logistic regression model
        """
        df = df_orig.copy()
        max_rate_impute = 0.20329
        term_prob_cols = [f'pred_term_{term}' for term in range(2, 8)]
        #Missing imputation
        for term, rate_col in zip(range(2, 8), rate_cols):
            df[f'min_rate_{term}_impute'] = np.where(df[rate_col].notnull(), df[rate_col], max_rate_impute)
        #Rate diff
        for idx, term in enumerate(range(2, 6)):
            df[f'min_rate_diff_impute_{term+1}_{term}'] = df[f'min_rate_{term+1}_impute'] - df[f'min_rate_{term}_impute']
        #Missing indicator
        for term, rate_col in zip(range(2, 8), rate_cols):
            df[f'min_rate_{term}_missing_ind'] = np.where(df[rate_col].isnull(), 1, 0)
        #Calculate logistic regression prediction
        first_term = range(2, 8)[0]
        df[f'pred_term_{first_term}'] = np.exp((term_coef.loc[get_segment_params(term_coef.index, first_term)].values.reshape(-1) * \
                                                df[[f'min_rate_{first_term}_impute', f'min_rate_{first_term}_missing_ind']].values).sum(axis=1))
        for term in range(2, 8)[1:]:
            coef_term = term_coef.loc[get_segment_params(term_coef.index, term)].values.reshape(-1)
            df[f'pred_term_{term}'] = np.exp((coef_term[1:] * \
                                              df[['credit_score', 'combined_organic', 'MKT-DM', 'Rest', 'requested_amount'] \
                                                 + [f'min_rate_diff_impute_{temp_term + 1}_{temp_term}' for temp_term in range(2, 6)] \
                                                 + [f'min_rate_{term}_impute', f'min_rate_{term}_missing_ind']].values).sum(axis=1) \
                                                 + coef_term[0])
        df = adjust_term_rate(df, rate_cols)
        return df[['id'] + term_prob_cols]
    