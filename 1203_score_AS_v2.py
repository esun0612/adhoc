#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  3 09:14:19 2019

@author: esun

v2: score on 12/09 use data until 12/05
"""
from os.path import join as pjoin
import numpy as np
import pandas as pd
import sys
import glob
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/erika_utils')
sys.path.insert(0, '/home/ec2-user/SageMaker/erika_git/pl_model')
from util_model import score_model, load_json, score_term_model, Header
from util_data import add_signed_ind, clean_str, esave, eread, flat_term_rate_dat, flat_rate, query_sofidw, query_snowflake, impute_rate, divide_train_oos 
from util_data import calc_monthly_payments, calc_payments, check_trans_to_list, change_cols
from header_to_model import merge_all_pred, report_performance_metrics

df = query_snowflake(qry)
qry = """
SELECT
af.id,
af.applicant_id,
af.date_signed,
af.date_doc_upload,
af.date_start,
af.date_fund,
af.initial_term,
af.interest_rate,
af.interest_rate_type,
af.requested_amount,
af.gross_income,
af.app_created_via_mobile,
af.g_program,
af.g_grad_year,
af.credit_score,
af.employer_name,
af.years_of_experience,
af.pl_funds_use,
af.tax_burden_amount,
af.consolidated_channel,
af.attr_affiliate_referrer,
af.campaign_welcome_bonus,
af.housing_status,
acaf.pil0438,
af.registration_date,
af.revolving_credit_amount,
af.ug_ctgry,
af.ug_grad_year,
af.ug_program,
af.member_indicator,
acaf.iln5020,
acaf.alx8220,
acaf.all8020,
acaf.iln5820,
acaf.all0416,
acaf.REV0416,
acaf.mtf5020,
acaf.mta5020,
acaf.mta5830,
acaf.mtj5030,
acaf.mtj5820,
acaf.all5820,
acaf.all7516,
acaf.all8220,
acaf.all5020,
acaf.bcc7110,
acaf.bcc8322,
acaf.bcx3421,
acaf.bcx3422,
acaf.bcx5020,
acaf.bcx5320,
acaf.bcx7110,
acaf.rev5020,
acaf.rev8320,
acaf.rta7300,
acaf.rtr5030,
acaf.all7517,
acaf.iln0416,
acaf.mta0416,
acaf.mta1370,
acaf.mta2800,
acaf.mta8150,
acaf.mta8153,
acaf.mta8157,
acaf.mta8160,
acaf.mtf0416,
acaf.mtf4260,
acaf.mtf8166,
acaf.mts5020,
af.free_cash_flow_pre,
CASE WHEN af.date_doc_upload IS NOT NULL THEN 1 ELSE 0 END AS docs_ind,
CASE WHEN af.date_signed IS NOT NULL THEN 1 ELSE 0 END AS signed_ind,
coalesce(af.tier, oo.tier, ooo.tier) AS tier,
coalesce(oo.champion_challenger_name,ooo.champion_challenger_name) as challenger_name,
oo.rate_2,
oo.rate_3,
oo.rate_4,
oo.rate_5,
oo.rate_6,
oo.rate_7,
coalesce(oo.offer_date,ooo.offer_date) offer_date
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
LEFT JOIN                     
    (SELECT
    paf.application_id,
    min(o.champion_challenger_name) as champion_challenger_name,
    min(CASE WHEN p.product_term = 2 THEN ofr.min_rate END) AS rate_2,
    min(CASE WHEN p.product_term = 3 THEN ofr.min_rate END) AS rate_3,
    min(CASE WHEN p.product_term = 4 THEN ofr.min_rate END) AS rate_4,
    min(CASE WHEN p.product_term = 5 THEN ofr.min_rate END) AS rate_5,
    min(CASE WHEN p.product_term = 6 THEN ofr.min_rate END) AS rate_6,
    min(CASE WHEN p.product_term = 7 THEN ofr.min_rate END) AS rate_7,
    min(REGEXP_SUBSTR(min_tier_code,'[1-9]')) AS tier,
    min(d.CALENDAR_DATE) AS offer_date
    FROM sofidw.product_application_facts paf
    JOIN sofidw.underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf.initial_uw_id, 0))
    JOIN sofidw.offer_facts ofr ON ofr.underwriting_info_id = ui.underwriting_info_id
    JOIN sofidw.offer_details o on ofr.offer_details_id = o.offer_details_id
    JOIN sofidw.products p ON p.product_id = ofr.product_id
    JOIN sofidw.dates d ON d.DATE_ID = ofr.ASOF_DATE_ID
    JOIN dwmart.applications_file af2 on af2.dw_application_id = paf.application_id AND af2.requested_amount > ofr.min_amount AND af2.requested_amount <= ofr.max_amount
    WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
    GROUP BY paf.APPLICATION_ID
    ) oo ON oo.application_id = af.dw_application_id
LEFT JOIN           
    (SELECT
    paf.application_id,
    min(o.champion_challenger_name) as champion_challenger_name,
    min(REGEXP_SUBSTR(min_tier_code,'[1-9]')) AS tier,
    min(d.CALENDAR_DATE) AS offer_date
    FROM sofidw.product_application_facts paf
    JOIN sofidw.underwriting_info ui ON ui.underwriting_info_id =
    coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0),
    NULLIF(paf.initial_uw_id, 0))
    JOIN sofidw.offer_facts ofr ON ofr.underwriting_info_id = ui.underwriting_info_id
    JOIN sofidw.offer_details o on ofr.offer_details_id = o.offer_details_id
    JOIN sofidw.products p ON p.product_id = ofr.product_id
    JOIN sofidw.dates d ON d.DATE_ID = ofr.ASOF_DATE_ID
    WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
    group by paf.application_id
    )ooo ON ooo.application_id = af.dw_application_id
WHERE
af.current_decision = 'ACCEPT'
AND af.date_start >= '2018-04-01'
AND af.application_type in ('PL')
"""
dat_dir = '/home/ec2-user/SageMaker/adhoc/AS_V2'
esave(df, pjoin(dat_dir, '1203_AS_score.feather'))
df = eread(pjoin(dat_dir, '1203_AS_score.feather'))
df['offer_date'] = pd.to_datetime(df['offer_date'])
df = df[df['offer_date'] >= '2019-11-15']
df['challenger_name'].value_counts()
O.G. Champion      4648
O.G. Challenger    3119
Rothschild         3073
Morgan             2593
Chase              2540

df = df[df['challenger_name'] == 'Morgan']

impute_rate_dict={'min_rate_2': 0.16677,
                 'min_rate_3': 0.16677,
                 'min_rate_4': 0.17436,
                 'min_rate_5': 0.18133,
                 'min_rate_6': 0.15313,
                 'min_rate_7': 0.16115}

def process(df, impute_rate_dict):
    #Member indicator, grad to 0/1 variable
    df['grad'] = np.where(df['g_program'].notnull(), 1, 0)
    #Define date related variables
    df['date_start'] = pd.to_datetime(df['date_start'])
    min_start_date = df['date_start'].min()
    df['date_start_year'] =  df['date_start'].dt.year
    df['date_start_month'] = df['date_start'].dt.month
    df['date_start_day'] = df['date_start'].dt.day
   
    df['attr_affiliate_referrer'] = clean_str(df['attr_affiliate_referrer'])
    g_program_list = ['Business - MBA', 'Other', 'Law - JD', 'Engineering/Computer Science - MS or higher',
                                  'Business - MS or MA or higher', 'Social Sciences - MS or MA or higher']
    channel_list = ['www.sofi.com', 'Affiliate', 'SEM', 'Organic Search', 'MKT-DM', 'P2P']
    affiliate_list = ['www.sofi.com', 'www.google.com', '2019_pl_sofi_mail', 'credit_karma', 'lending_tree', 'nerdwallet']
    df = cut_cat_var(df, 'g_program', g_program_list)
    df = cut_cat_var(df, 'consolidated_channel', channel_list)
    df = cut_cat_var(df, 'attr_affiliate_referrer', affiliate_list)
    
    #Create indicator variables for selected each term or not
    PL_terms = [2, 3, 4, 5, 6, 7]
    for term in PL_terms:
        df[f'initial_term_{term}'] = np.where((df['initial_term'] == term), 1, 0)
    #Map interest_rate to individual term columns
    df = flat_rate(df, PL_terms)
    #Consolidate tier columns
    df['tier_pre'] = df['tier']

    df['g_grad_year'] = df['date_start'].dt.year - df['g_grad_year']
    df['ug_grad_year'] = df['date_start'].dt.year - df['ug_grad_year']
    #registration_date: date_start - var
    df['registration_date'] = pd.to_datetime(df['registration_date'])
    df['registration_date'] = (df['date_start'] - df['registration_date']).dt.days
    
    df.rename(columns={f'rate_{term}': f'min_rate_{term}' for term in PL_terms}, inplace=True)
    df, _ = impute_rate(df, rate_cols=[f'min_rate_{term}' for term in PL_terms], impute_rate_dict=impute_rate_dict)
    rate_cols = [f'min_rate_{term}' for term in range(2,8)]
    df['min_all_rates'] = df[rate_cols].min(axis=1) #Minimum of all rates
    
    for term in PL_terms:
        df[f'monthly_payment_{term}'] = calc_monthly_payments(df['requested_amount'], term, df[f'min_rate_{term}_orig'])
        df[f'free_cash_flow_post_{term}'] = df['free_cash_flow_pre'] - df[f'monthly_payment_{term}']
    monthly_payment_cols = [f'monthly_payment_{term}' for term in PL_terms]
    df['min_monthly_payment'] = df[monthly_payment_cols].min(axis=1)
    df['min_monthly_payment'].fillna(df['min_monthly_payment'].max(), inplace=True)
    df['free_cash_flow_post_max'] = df['free_cash_flow_pre'] - df['min_monthly_payment']
    
    for term in range(2, 7):
        df[f'min_rate_diff_{term+1}_{term}'] = df[f'min_rate_{term+1}'] - df[f'min_rate_{term}']
    return df

df2 = process(df, impute_rate_dict)
df2.describe().transpose().to_csv(pjoin(dat_dir, 'AS_stat.csv'))
PL_terms = [2, 3, 4, 5, 6, 7]
df2 = add_signed_ind(df2, terms=PL_terms, remove_mix_labels=False, filterdays=365)
df2.to_csv(pjoin(dat_dir, 'AS.csv'), index=None)

model_dir = '/home/ec2-user/SageMaker/round_2/experiments/7g_2019-11-26_16-29-29'
out_dir = make_output_dir(dat_dir, 'AS', make=True, make_time_stamp_dir=False)
#Score rate
raw_rate = score_model(glob.glob(pjoin(model_dir, 'model_rate*.txt'))[0], pjoin(out_dir, 'rate_pred_raw.csv'), 
            data_file=pjoin(dat_dir, 'AS.csv'), header_file=glob.glob(pjoin(model_dir, '*_rate_header.csv'))[0])
pred_rate = merge_rate_model(pjoin(dat_dir, 'AS.csv'), pjoin(out_dir, 'rate_pred_raw.csv'), pjoin(out_dir, 'rate_pred.csv'))

#Score term
score_model(glob.glob(pjoin(model_dir, 'model_term*.txt'))[0], pjoin(out_dir, 'term_pred.csv'), 
            data_file=pjoin(dat_dir, 'AS.csv'), header_file=glob.glob(pjoin(model_dir, '*_term_header.csv'))[0])

params_rate = load_json(glob.glob(pjoin(model_dir, 'params_rate_*.json'))[0]) 
terms = params_rate['terms']
rate_cols = ['{0}_{1}'.format(params_rate['term_col_prefix'], term) for term in terms]
pred_term_col = 'term_wgt_prob'
pred_term_prob_cols = [col + '_prob_adj' for col in rate_cols]
rate_orig_cols = [col + '_orig' for col in rate_cols]
target_term_col = 'initial_term'
target_term_prob_cols = [f'{target_term_col}_{term}' for term in terms]

term_score = score_term_model(data_file=pjoin(dat_dir, 'AS.csv'), raw_model_output_file=pjoin(out_dir, 'term_pred.csv'), 
                 output_file=pjoin(out_dir, 'pred_term_final.feather'), 
                 impute_rate_cols=rate_cols, orig_rate_cols=rate_orig_cols, terms=terms, target_term_prob_cols=target_term_prob_cols)

#Evaluation
act_term_prob_cols = ['signed_ind_{}'.format(term) for term in terms]  
rate_map_cols = [f'rate_map_{term}' for term in terms]
df_final = merge_all_pred(pjoin(out_dir, 'rate_pred.csv'), pjoin(out_dir, 'pred_term_final.feather'), 
                          pjoin(dat_dir, 'AS.csv'), act_term_prob_cols, rate_map_cols, output_file=pjoin(out_dir, 'pred_final.feather'))

final_result = report_performance_metrics([df_final], ['oot'], terms, act_term_prob_cols,
                                          pred_term_prob_cols, rate_map_cols, rate_cols, 'tier', 'tier_pre')
final_result.to_csv(pjoin(out_dir, "final_metrics.csv"))

df2['interest_rate_type'].value_counts()
Out[246]: 
FIXED       1058
VARIABLE      20

df = eread(pjoin(out_dir, 'pred_final.feather'))

a = []
for term in range(2, 8):
    result = df[(df['initial_term']==term) & (df['signed_ind']==1)]['requested_amount'].sum() / df[df['signed_ind']==1]['requested_amount'].sum()
    print(df[(df['initial_term']==term) & (df['signed_ind']==1)]['requested_amount'].sum(), result)
    a.append(result)
    
df[df['signed_ind']==1].groupby('initial_term')['id'].count()
initial_term
2.0     66
3.0     91
4.0     68
5.0    124
6.0     17
7.0     22

oot = pd.read_csv("/home/ec2-user/SageMaker/round_2/data/PL_rate_oot_7g.csv")
oot[oot['signed_ind']==1].groupby('initial_term')['id'].count()
initial_term
2.0     886
3.0    2215
4.0    1716
5.0    3756
6.0     259
7.0     648