#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 10:34:05 2020

@author: esun

Investigate on channel distribution
"""

#channel distribution after 02/20
df = eread('../adhoc/BA_BB/ba_bb_data.csv')
df['rate_avi_ind'] = np.all(df[[f'rate_{term}' for term in range(2, 8)]].isnull(), axis=1)
df = process_1(df)

df['consolidated_channel_model'].value_counts() / df.shape[0]
"""
combined_organic    0.462068
Affiliate           0.254009
SEM                 0.164679
MKT-DM              0.079461
Rest                0.039782
"""
df = df.loc[df['rate_avi_ind']==0]
df.groupby('consolidated_channel_model')['requested_amount'].sum() / df['requested_amount'].sum()
"""
Affiliate           0.266620
MKT-DM              0.084212
Rest                0.035896
SEM                 0.163691
combined_organic    0.449580
"""

df = eread('../pl_opt/opt_0204/data/opt_tier_dist.csv')
df['consolidated_channel_model'].value_counts() / df.shape[0]
"""
combined_organic    0.443657
Affiliate           0.239334
SEM                 0.154778
MKT-DM              0.113625
Rest                0.048605
"""
df.groupby('consolidated_channel_model')['requested_amount'].sum() / df['requested_amount'].sum()
"""
Affiliate           0.243319
MKT-DM              0.125654
Rest                0.047455
SEM                 0.150829
combined_organic    0.432743
"""

#doc upload wac
df = add_du_ind(df)
def cal_du_wac(df):
    print (df.shape)
    return (df['requested_amount'] * df['du_ind'] * df['interest_rate']).sum() / (df['requested_amount'] * df['du_ind']).sum()

cal_du_wac(df[(df['challenger_name'] == 'O.G. Champion') & (df['consolidated_channel_model'] == 'Affiliate')])
#0.11298328910911591
cal_du_wac(df[(df['challenger_name'] == 'Morgan') & (df['consolidated_channel_model'] == 'Affiliate')])
#0.11069190015867503
cal_du_wac(df[(df['challenger_name'] == 'O.G. Challenger') & (df['consolidated_channel_model'] == 'Affiliate')])
#0.11378938391303935

print(cal_du_wac(df[(df['challenger_name'] == 'O.G. Champion')]))
#0.11652038999402779
print(cal_du_wac(df[(df['challenger_name'] == 'Morgan')]))
#0.11561949104706165
print(cal_du_wac(df[(df['challenger_name'] == 'O.G. Challenger')]))
#0.11945087175518254


