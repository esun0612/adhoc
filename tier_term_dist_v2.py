#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 11:30:55 2019

@author: esun
"""

#10/15 - 11/14
octo = eread('/home/ec2-user/SageMaker/adhoc/AS_historical_v2/AS.csv')
tier_col = 'tier_pre'
octo.groupby(tier_col)['requested_amount'].sum() / octo['requested_amount'].sum()
octo_pred = eread('/home/ec2-user/SageMaker/adhoc/AS_historical_v2/pred_final.feather')
cal_conv_by_tier(octo_pred)

#AS
df = pd.read_csv('/home/ec2-user/SageMaker/adhoc/AS.csv')
df2 = df.dropna(subset=[f'min_rate_{term}_orig' for term in range(2, 8)], how='all')
df3 = df2[df2['date_start'] >= '2019-11-15']
df_final = eread('/home/ec2-user/SageMaker/adhoc/AS/pred_final.feather')
df_final = pd.merge(df_final, df2[['id']], on='id')
df_final_2 = pd.merge(df_final, df3[['id']], on='id')
final_result = report_performance_metrics([df_final], ['oot'], terms, act_term_prob_cols,
                                          pred_term_prob_cols, rate_map_cols, rate_cols, 'tier', 'tier_pre')
final_result.to_csv(pjoin('/home/ec2-user/SageMaker/adhoc/AS', "final_metrics_exclude.csv"))
final_result = report_performance_metrics([df_final_2], ['oot'], terms, act_term_prob_cols,
                                          pred_term_prob_cols, rate_map_cols, rate_cols, 'tier', 'tier_pre')
final_result.to_csv(pjoin('/home/ec2-user/SageMaker/adhoc/AS', "final_metrics_exclude_date.csv"))
cal_conv_by_tier(df_final)
cal_conv_by_tier(df)

def cal_tier_term_dist(df, term_prob_cols, sign_col, adj_rate_cols, loan_amt_col='requested_amount', tier_col='tier_pre'):
    """Calculate tier and term distribution
    """
    df['signed_loan_amt'] = df[loan_amt_col] * df[sign_col]

    
def cal_term_dist(df, term_prob_cols, adj_rate_cols, signed_loam_amt_col='signed_loan_amt'):
    """Calculate term distribution
    """
    signed_loan_amt_by_term = np.expand_dims(df['signed_loan_amt'].values, axis=1) * df[term_prob_cols].values
    term_dist = signed_loan_amt_by_term.sum(axis=0) / df['signed_loan_amt'].sum()
    return term_dist
    
    
def cal_conv_by_tier(df, tier_col='tier_pre'):
    result = []
    for tier in range(1, 8):
        df_tier = df[df[tier_col] == tier]
        result.append((df_tier['rate_pred'] * df_tier['requested_amount']).sum() / df_tier['requested_amount'].sum())
    return pd.Series(result)

#PSI
octo = eread('/home/ec2-user/SageMaker/adhoc/AS_historical_v2/AS.csv')
nov = pd.read_csv('/home/ec2-user/SageMaker/adhoc/AS.csv')
nov = nov.merge(df2[['id']], on='id')
nov.groupby(tier_col)['requested_amount'].sum() / nov['requested_amount'].sum()

header = Header(glob.glob(pjoin('/home/ec2-user/SageMaker/round_2/experiments/7g_2019-11-26_16-29-29', '*_rate_header.csv'))[0])
octo_tier_4 = octo[octo['tier_pre'] == 4]
nov_tier_4 = nov[nov['tier'] == 4]
psi_dict = {}
for col in header.num_cols:
    try:
        psi_dict[col] = cal_psi(octo_tier_4, nov_tier_4, col)
    except Exception:
        pass
df_psi = pd.DataFrame(psi_dict, index=[0])
df_psi = df_psi.transpose()
df_psi = df_psi.sort_values(by=[0], ascending=False)

for var in df_psi.index[1:4]:
    print(var)
    print(cal_psi(octo_tier_4, nov_tier_4, var, return_table=True))
    
#Tier distribution by time
result = []
result_cnt = []
for date_start_start, date_start_end in zip(['2019-10-15', '2019-10-22', '2019-10-29', '2019-11-05', '2019-11-08'], ['2019-10-21', '2019-10-28', '2019-11-04', '2019-11-07', '2019-11-14']):
    df_tmp = octo[(octo['date_start'] <= date_start_end) & (octo['date_start'] >= date_start_start)]
    tier_dist = list(df_tmp.groupby(tier_col)['requested_amount'].sum() / df_tmp['requested_amount'].sum())
    tier_cnt_dist = list(df_tmp.groupby(tier_col)['id'].count() / df_tmp.shape[0])
    result.append(tier_dist)
    result_cnt.append(tier_cnt_dist)
#pd.DataFrame(result)

for date_start_start, date_start_end in zip(['2019-11-15', '2019-11-22'], ['2019-11-21', '2019-12-01']):
    df_tmp = nov[(nov['date_start'] <= date_start_end) & (nov['date_start'] >= date_start_start)]
    tier_dist = list(df_tmp.groupby(tier_col)['requested_amount'].sum() / df_tmp['requested_amount'].sum())
    result.append(tier_dist)
    result_cnt.append(tier_cnt_dist)
print(pd.DataFrame(result).transpose())
print(pd.DataFrame(result_cnt).transpose())




