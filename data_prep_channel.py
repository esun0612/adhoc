#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 16:03:01 2020

@author: esun

Prepare channel label
"""
import numpy as np
from os.path import join as pjoin
import mdsutils
import pandas as pd
from util_data import eread

#Query campaign_id
ath = mdsutils.AthenaClient(database='datalake_production_core')
df = ath.query_to_df("""select distinct te.target_id, t.campaign_id
                        from tracking_event as te
                        join tracking as t on te.tracking_id = t.id
                        where te.target_type = 'APP' and
                        te.created_dt >= date '2018-01-01' and te.created_dt <= date '2020-02-04'
                        """)
dat_dir = '../pl_opt/opt_0204/data'                        
df.to_csv(pjoin(dat_dir, 'compaign_id.csv'), index=None)

#Merge with training data
df_campaign = pd.read_csv(pjoin(dat_dir, 'compaign_id.csv'))
df_campaign_dedup = df_campaign.drop_duplicates('target_id')
train = eread(pjoin(dat_dir, 'train.csv'))
df = pd.merge(train, df_campaign_dedup, how='left', left_on='id', right_on='target_id')
#Create API indicator by campaign_id
api_campaign_list = [1420, 1418, 3156,1420,1420,1420,2862,1420,2140,2293,1420,1420,1420,1420,1420,1420,1420,1414,1420,2260,1420,2210,1420,597]
df['api_ind'] = np.where(df['campaign_id'].isin(api_campaign_list), 1, 0)
df['k_channel'] = np.where(df['consolidated_channel'] == 'MKT-DM', 'DM',
                  np.where((df['attr_affiliate_referrer'] == 'lending_tree') & (df['api_ind'] == 1), 'LT_API',
                  np.where((df['attr_affiliate_referrer'] == 'credit_karma') & (df['api_ind'] == 1), 'CK_API',
                  np.where((df['consolidated_channel'] == 'Affiliate') & (df['api_ind'] == 1), 'other_API',
                  np.where(df['consolidated_channel'] == 'Affiliate', 'other_non_API',
                  np.where(df['consolidated_channel'].isin(['www.sofi.com', 'Organic Search']), 'combined_organic', 'other')
                  )))))
"""
combined_organic	37%
non_api	19%
api	9% - lt 3.4%, ck 3.1%
DM	15%
REST	 19%
"""