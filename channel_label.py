#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 26 13:50:57 2020

@author: esun

Josh Olson:snowflake:  4:13 PM
@here here’s the proposed logic for pricing_attribution
if (channel IS "Sofi Mail" AND direct_mail IS active) {
	attr = "DM-Prequal"
} elif (target_type IS "PARTNER_OFFER") {
	if (user_tracking_channel IS "Credit Karma") {
		attr = "CK-API"
	} elif (user_tracking_channel IS "Lending Tree") {
		attr = "LT-API"
	} else {
		attr = "All other affiliate - API"
	}
} elif (consolidated_channel IN {AFFILIATE, STRATEGIC_PARTNER})
	attr = "All other affiliate - Non API"
} elif (consolidated_channel IS SOFI_DOMAIN) {
	attr = "Sofi.com"
} else {
	attr = "All Others"
}
"""

