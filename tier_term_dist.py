#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 12:07:37 2019

@author: esun
"""
from util_analysis import plot_hist, gen_stat
#Oct
df = eread('/home/ec2-user/SageMaker/adhoc/check_optimization/as_with_rate.feather')
df = add_signed_ind(df, terms=PL_terms)
df['t_tier'] = df[[f'tier_{term}' for term in range(2,8)]].min(axis=1)    
#df.groupby(tier_col)['requested_amount'].sum() / df['requested_amount'].sum()
tier_col = 't_tier'
result = []
for tier in range(1, 8):
    df_tier = df[df[tier_col] == tier]
    result.append(df_tier[df_tier['signed_ind'] == 1]['requested_amount'].sum() / df_tier['requested_amount'].sum())

df_score = eread('/home/ec2-user/SageMaker/adhoc/check_optimization/pred_final.feather')
df = pd.merge(df_score, df[['id', 't_tier']], on='id')
tier_col = 't_tier'
df[tier_col] = df[tier_col].astype(int)
result = []
for tier in range(1, 8):
    df_tier = df[df[tier_col] == tier]
    result.append((df_tier['rate_pred'] * df_tier['requested_amount']).sum() / df_tier['requested_amount'].sum())
pd.Series(result)

plot_hist(df[df[tier_col] == 6]['gross_income'], tick_fontsize=8, hist_cat_tick_fontsize=8)
gen_stat(df[df[tier_col] == 6], 'gross_income')

#Nov
df = eread('/home/ec2-user/SageMaker/adhoc/1203_AS_score.feather')
df['offer_date'] = pd.to_datetime(df['offer_date'])
df = df[df['offer_date'] >= '2019-11-15']
df = df[df['challenger_name'] == 'Morgan']
#df.groupby('tier')['requested_amount'].sum() / df['requested_amount'].sum()

df = eread('/home/ec2-user/SageMaker/adhoc/AS/pred_final.feather')
tier_col = 'tier'
df[tier_col] = df[tier_col].astype(int)
result = []
for tier in range(1, 8):
    df_tier = df[df[tier_col] == tier]
    result.append((df_tier['rate_pred'] * df_tier['requested_amount']).sum() / df_tier['requested_amount'].sum())
pd.Series(result)

dat_dir = '/home/ec2-user/SageMaker/adhoc'
octo = pd.read_csv(pjoin(dat_dir, 'oct.csv'))
nov = pd.read_csv(pjoin(dat_dir, 'nov.csv'))

octo[octo['t_tier'] == 6].describe().transpose().to_csv(pjoin(dat_dir, 'oct_stat.csv'))
nov[nov['tier'] == 6].describe().transpose().to_csv(pjoin(dat_dir, 'nov_stat.csv'))


octo = pd.read_csv('/home/ec2-user/SageMaker/adhoc/check_optimization/AS.csv')
nov = pd.read_csv('/home/ec2-user/SageMaker/adhoc/AS.csv')

header = Header(glob.glob(pjoin('/home/ec2-user/SageMaker/round_2/experiments/7g_2019-11-26_16-29-29', '*_rate_header.csv'))[0])
octo_tier_4 = octo[octo['tier_pre'] == 4]
nov_tier_4 = nov[nov['tier'] == 4]
psi_dict = {}
for col in header.num_cols:
    try:
        psi_dict[col] = cal_psi(octo_tier_4, nov_tier_4, col)
    except Exception:
        pass
df_psi = pd.DataFrame(psi_dict, index=[0])
df_psi = df_psi.transpose()
df_psi.sort_values(by=[0], ascending=False)

for var in ['min_rate_3','min_all_rates','min_rate_4','requested_amount','min_rate_2']:
    print(var)
    print(cal_psi(octo_tier_4, nov_tier_4, var, return_table=True))