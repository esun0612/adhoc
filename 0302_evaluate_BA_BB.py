#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 16:50:37 2020

@author: esun

Evaluate performance for BA and BB grid
"""

from os.path import join as pjoin
import numpy as np
import pandas as pd
import sys
import glob
sys.path.insert(0, '/Users/esun/Documents/erika_git/erika_utils')
sys.path.insert(0, '../erika_git/optimization/opt_utils')

from util_data import query_snowflake
from util_data import clean_str, cut_cat_var, flat_rate, eread, add_signed_ind

#1. query data
#start_date = '2020-02-20'
#end_date = '2020-02-27'

df = query_snowflake(qry)
qry = f"""
SELECT
af.id,
af.applicant_id,
af.date_signed,
af.date_doc_upload,
af.date_start,
af.date_fund,
af.initial_term,
af.interest_rate,
af.interest_rate_type,
af.requested_amount,
af.gross_income,
af.app_created_via_mobile,
af.g_program,
af.g_grad_year,
af.credit_score,
af.employer_name,
af.years_of_experience,
af.pl_funds_use,
af.tax_burden_amount,
af.consolidated_channel,
af.attr_affiliate_referrer,
af.campaign_welcome_bonus,
af.housing_status,
acaf.pil0438,
af.registration_date,
af.revolving_credit_amount,
af.ug_ctgry,
af.ug_grad_year,
af.ug_program,
af.member_indicator,
acaf.iln5020,
acaf.alx8220,
acaf.all8020,
acaf.iln5820,
acaf.all0416,
acaf.REV0416,
acaf.mtf5020,
acaf.mta5020,
acaf.mta5830,
acaf.mtj5030,
acaf.mtj5820,
acaf.all5820,
acaf.all7516,
acaf.all8220,
acaf.all5020,
acaf.bcc7110,
acaf.bcc8322,
acaf.bcx3421,
acaf.bcx3422,
acaf.bcx5020,
acaf.bcx5320,
acaf.bcx7110,
acaf.rev5020,
acaf.rev8320,
acaf.rta7300,
acaf.rtr5030,
acaf.all7517,
acaf.iln0416,
acaf.mta0416,
acaf.mta1370,
acaf.mta2800,
acaf.mta8150,
acaf.mta8153,
acaf.mta8157,
acaf.mta8160,
acaf.mtf0416,
acaf.mtf4260,
acaf.mtf8166,
acaf.mts5020,
af.free_cash_flow_pre,
CASE WHEN af.date_doc_upload IS NOT NULL THEN 1 ELSE 0 END AS docs_ind,
CASE WHEN af.date_signed IS NOT NULL THEN 1 ELSE 0 END AS signed_ind,
coalesce(af.tier, oo.tier, ooo.tier) AS tier,
coalesce(oo.champion_challenger_name,ooo.champion_challenger_name) as challenger_name,
oo.rate_2,
oo.rate_3,
oo.rate_4,
oo.rate_5,
oo.rate_6,
oo.rate_7,
coalesce(oo.offer_date,ooo.offer_date) offer_date
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
LEFT JOIN                     
    (SELECT
    paf.application_id,
    min(o.champion_challenger_name) as champion_challenger_name,
    min(CASE WHEN p.product_term = 2 THEN ofr.min_rate END) AS rate_2,
    min(CASE WHEN p.product_term = 3 THEN ofr.min_rate END) AS rate_3,
    min(CASE WHEN p.product_term = 4 THEN ofr.min_rate END) AS rate_4,
    min(CASE WHEN p.product_term = 5 THEN ofr.min_rate END) AS rate_5,
    min(CASE WHEN p.product_term = 6 THEN ofr.min_rate END) AS rate_6,
    min(CASE WHEN p.product_term = 7 THEN ofr.min_rate END) AS rate_7,
    min(REGEXP_SUBSTR(min_tier_code,'[1-9]')) AS tier,
    min(d.CALENDAR_DATE) AS offer_date
    FROM sofidw.product_application_facts paf
    JOIN sofidw.underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf.initial_uw_id, 0))
    JOIN sofidw.offer_facts ofr ON ofr.underwriting_info_id = ui.underwriting_info_id
    JOIN sofidw.offer_details o on ofr.offer_details_id = o.offer_details_id
    JOIN sofidw.products p ON p.product_id = ofr.product_id
    JOIN sofidw.dates d ON d.DATE_ID = ofr.ASOF_DATE_ID
    JOIN dwmart.applications_file af2 on af2.dw_application_id = paf.application_id AND af2.requested_amount > ofr.min_amount AND af2.requested_amount <= ofr.max_amount
    WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
    GROUP BY paf.APPLICATION_ID
    ) oo ON oo.application_id = af.dw_application_id
LEFT JOIN           
    (SELECT
    paf.application_id,
    min(o.champion_challenger_name) as champion_challenger_name,
    min(REGEXP_SUBSTR(min_tier_code,'[1-9]')) AS tier,
    min(d.CALENDAR_DATE) AS offer_date
    FROM sofidw.product_application_facts paf
    JOIN sofidw.underwriting_info ui ON ui.underwriting_info_id =
    coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0),
    NULLIF(paf.initial_uw_id, 0))
    JOIN sofidw.offer_facts ofr ON ofr.underwriting_info_id = ui.underwriting_info_id
    JOIN sofidw.offer_details o on ofr.offer_details_id = o.offer_details_id
    JOIN sofidw.products p ON p.product_id = ofr.product_id
    JOIN sofidw.dates d ON d.DATE_ID = ofr.ASOF_DATE_ID
    WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
    group by paf.application_id
    )ooo ON ooo.application_id = af.dw_application_id
WHERE
af.current_decision = 'ACCEPT'
AND af.date_start >= '2018-04-01'
AND af.application_type in ('PL')
"""

#2. clean data
df['offer_date'] = pd.to_datetime(df['offer_date'])
df2 = df[(df['offer_date'] >= '2020-02-20') & (df['offer_date'] <= '2020-02-27')]
df2['challenger_name'].value_counts()
df2.to_csv('/Users/esun/Documents/erika_git/adhoc/data/ba_bb_data.csv', index=None)
"""
O.G. Challenger    3367
O.G. Champion      3217
Morgan             3144
"""
#O.G. Challenger AY
#O.G. Champion   BA
#Morgan          BB

#3. Process data
def process_1(df):
    #Member indicator, grad to 0/1 variable
    df['member_indicator'] = df['member_indicator'].map({False: 0, True: 1})
    df['grad'] = np.where(df['g_program'].notnull(), 1, 0)
    #Define date related variables
    df['date_start'] = pd.to_datetime(df['date_start'])
    min_start_date = df['date_start'].min()
    df['date_start_year'] =  df['date_start'].dt.year
    df['date_start_month'] = df['date_start'].dt.month
    df['date_start_day'] = df['date_start'].dt.day
    df['date_start_weekday'] = df['date_start'].dt.weekday
    df['ref_month'] = df['date_start_year'] * 12 + df['date_start_month'] - (min_start_date.year * 12 + min_start_date.month)
    df['ref_day'] = df['date_start'].dt.dayofyear + (df['date_start_year'] - min_start_date.year) * 365
    #Clean affiliate variable
    df['attr_affiliate_referrer'] = clean_str(df['attr_affiliate_referrer'])
    df['attr_affiliate_referrer_new'] = np.where(df['attr_affiliate_referrer'].isin(['www.sofi.com','www.google.com','credit_karma','lending_tree']),
                                                     df['attr_affiliate_referrer'], 'other')
    #Create indicator variables for selected each term or not
    for term in PL_terms:
        df[f'initial_term_{term}'] = np.where((df['initial_term'] == term), 1, 0)
    #Map interest_rate to individual term columns
    df = flat_rate(df, PL_terms)
    #Consolidate tier columns
    df['tier_pre'] = df['tier']
    
    #Cut and group categorical variables
    g_program_list = ['Business - MBA', 'Other', 'Law - JD', 'Engineering/Computer Science - MS or higher',
                                  'Business - MS or MA or higher', 'Social Sciences - MS or MA or higher']
    channel_list = ['www.sofi.com', 'Affiliate', 'SEM', 'Organic Search', 'MKT-DM', 'P2P']
    affiliate_list = ['www.sofi.com', 'www.google.com', '2019_pl_sofi_mail', 'credit_karma', 'lending_tree', 'nerdwallet']
    df = cut_cat_var(df, 'g_program', g_program_list)
    df = cut_cat_var(df, 'consolidated_channel', channel_list)
    df = cut_cat_var(df, 'attr_affiliate_referrer', affiliate_list)
    
    #Clean date variables
    df['date_start'] = pd.to_datetime(df['date_start'])
    df['g_grad_year'] = df['date_start'].dt.year - df['g_grad_year']
    df['ug_grad_year'] = df['date_start'].dt.year - df['ug_grad_year']
    #registration_date: date_start - var
    df['registration_date'] = pd.to_datetime(df['registration_date'])
    df['registration_date'] = (df['date_start'] - df['registration_date']).dt.days
    
    #add rate_diff columns
    df.rename(columns={f'rate_{term}': f'min_rate_{term}' for term in range(2, 8)}, inplace=True)
    for term in range(2, 7):
        df[f'min_rate_diff_{term+1}_{term}'] = df[f'min_rate_{term+1}'] - df[f'min_rate_{term}']
    
    #Consolidated_channel variable
    df['consolidated_channel_model'] = df['consolidated_channel']
    df['consolidated_channel_model'] = np.where(df['consolidated_channel'].isin(['www.sofi.com', 'Organic Search']), 'combined_organic', df['consolidated_channel_model'])
    df['consolidated_channel_model'] = np.where(df['consolidated_channel'] == 'P2P', 'Rest', df['consolidated_channel_model'])
    df_channel_dummy = pd.get_dummies(df['consolidated_channel_model'])
    df = pd.concat([df, df_channel_dummy], axis=1)
    
    return df

df = eread('../adhoc/BA_BB/ba_bb_data.csv')
df['rate_avi_ind'] = np.all(df[[f'rate_{term}' for term in range(2, 8)]].isnull(), axis=1)
df = process_1(df)
PL_terms = [2, 3, 4, 5, 6, 7]
df2 = add_signed_ind(df, terms=PL_terms, remove_mix_labels=False, filterdays=365)
df2[df2['challenger_name'] == 'O.G. Champion'].to_csv('../adhoc/BA_BB/ba_data.csv', index=None)
df2[df2['challenger_name'] == 'Morgan'].to_csv('../adhoc/BA_BB/bb_data.csv', index=None)

#Score GBM
rate_cols = [f'min_rate_{term}' for term in range(2, 8)]
ba_score = score_lightgbm(rate_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_elasticity_model.txt', 
                           term_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_term_model_with_diff.txt', 
                           rate_header_file='../pl_model_2020_01/lightgbm_model/round2_7c_rate_header.csv', 
                           term_header_file='../round_2/experiments/7h_2019-11-26_17-16-51/round2_7h_term_header.csv', 
                           rate_cols=rate_cols,
                           data_file='../adhoc/BA_BB/ba_data.csv',
                           output_file='../adhoc/BA_BB/ba_score.csv')
bb_score = score_lightgbm(rate_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_elasticity_model.txt', 
                           term_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_term_model_with_diff.txt', 
                           rate_header_file='../pl_model_2020_01/lightgbm_model/round2_7c_rate_header.csv', 
                           term_header_file='../round_2/experiments/7h_2019-11-26_17-16-51/round2_7h_term_header.csv', 
                           rate_cols=rate_cols,
                           data_file='../adhoc/BA_BB/bb_data.csv',
                           output_file='../adhoc/BA_BB/bb_score.csv')

#Score residual
ba_lr_score = score_residual(rate_model_file='../pl_opt/opt_0204/model/elasticity_coef_table_0204.csv', 
                             term_model_file='../pl_opt/opt_0204/new_mnlogit/Term_Selection_Coef_0219.csv', 
                             max_rate_impute=0.18983, 
                             residual_rate_model_file='../pl_opt/opt_0204/model_num_tree_tuning/residual_elasticity_model.txt',
                             residual_rate_header_file='../erika_git/optimization/residual_model/header_residual_rate_3vars.csv',
                             data_file='../adhoc/BA_BB/ba_data.csv', 
                             output_file='../adhoc/BA_BB/ba_lr_score.csv')

bb_lr_score = score_residual(rate_model_file='../pl_opt/opt_0204/model/elasticity_coef_table_0204.csv', 
                             term_model_file='../pl_opt/opt_0204/new_mnlogit/Term_Selection_Coef_0219.csv', 
                             max_rate_impute=0.18983, 
                             residual_rate_model_file='../pl_opt/opt_0204/model_num_tree_tuning/residual_elasticity_model.txt',
                             residual_rate_header_file='../erika_git/optimization/residual_model/header_residual_rate_3vars.csv',
                             data_file='../adhoc/BA_BB/bb_data.csv', 
                             output_file='../adhoc/BA_BB/bb_lr_score.csv')

score_rate_residual(lightgbm_model_file='../pl_opt/opt_0204/model_num_tree_tuning/residual_elasticity_model.txt', 
                    data_file='../pl_opt/opt_0204/data/opt_rate_pred.csv', 
                    header_file='../erika_git/optimization/residual_model/header_residual_rate_3vars.csv', 
                    output_file='../pl_opt/opt_0204/data/opt_rate_pred_final_num_tree_tuning.csv')


#Evaluation
ba_score = pd.read_csv('../adhoc/BA_BB/ba_score.csv')
ba_score['rate_pred'] = np.where(ba_score['rate_avi_ind']==1, 0, ba_score['rate_pred'])
pd.DataFrame(cal_basic_metrics(ba_score)).transpose()

bb_score = pd.read_csv('../adhoc/BA_BB/bb_score.csv')
bb_score['rate_pred'] = np.where(bb_score['rate_avi_ind']==1, 0, bb_score['rate_pred'])
pd.DataFrame(cal_basic_metrics(bb_score)).transpose()

ba_lr_score = pd.read_csv('../adhoc/BA_BB/ba_lr_score.csv')
ba_lr_score['rate_pred'] = np.where(ba_lr_score['rate_avi_ind']==1, 0, ba_lr_score['rate_pred'])
pd.DataFrame(cal_basic_metrics(ba_lr_score)).transpose()

bb_lr_score = pd.read_csv('../adhoc/BA_BB/bb_lr_score.csv')
bb_lr_score['rate_pred'] = np.where(bb_lr_score['rate_avi_ind']==1, 0, bb_lr_score['rate_pred'])
pd.DataFrame(cal_basic_metrics(bb_lr_score)).transpose()

def cal_basic_metrics(df):
    df = cal_N_D(df)
    N = df['N'].sum()
    D = df['D'].sum()
    wac = N / D
    pct_signed = df['rate_pred'].mean()
    return wac, pct_signed

#Actual
#1. 3 day DU conversion
df = eread('../adhoc/BA_BB/bb_data.csv')
df = add_3day_du_ind(df)
df['du_3day_ind'].mean()

def add_3day_du_ind(df):
    """Create 3 day DU indicator
    """
    df['date_start'] = pd.to_datetime(df['date_start'])
    df['date_du'] = pd.to_datetime(df['date_doc_upload'])
    df['du_3day_ind'] = np.where(df['date_du'] - df['date_start'] <= pd.Timedelta(3,'D'), 1, 0)
    return df

#2. doc upload wac
df = eread('../adhoc/BA_BB/ba_data.csv')
df = add_du_ind(df)
wac = (df['requested_amount'] * df['du_ind'] * df['interest_rate']).sum() / (df['requested_amount'] * df['du_ind']).sum()

def add_du_ind(df):
    """Create DU indicator
    """
    df['du_ind'] = np.where(df['date_doc_upload'].notnull(), 1, 0)
    return df

#3. extrapoloate 3 day DU conversion -> 30 day signed conversion
#Use data 01/01 - 01/31
start_date = '2020-01-01'
end_date = '2020-01-31'
product = f"'PL'"
date_range = f"af.date_start>='{start_date}' and af.date_start<='{end_date}'"

PL_qry = f"""
SELECT
af.id,
af.date_signed,
af.date_doc_upload,
af.date_start,
af.initial_term,
af.interest_rate,
af.requested_amount
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
WHERE af.application_type = {product} AND {date_range}
AND (af.interest_rate_type = 'FIXED' OR af.interest_rate_type IS NULL)
AND af.current_decision = 'ACCEPT'
"""
df = query_snowflake(PL_qry)
df.to_csv('/Users/esun/Documents/erika_git/adhoc/data/2020_01_data.csv', index=None)

df = pd.read_csv('../adhoc/BA_BB/2020_01_data.csv')
df = add_3day_du_ind(df)
df = add_30day_sign_ind(df)

def add_30day_sign_ind(df):
    """Create 3 day DU indicator
    """
    df['date_start'] = pd.to_datetime(df['date_start'])
    df['date_signed'] = pd.to_datetime(df['date_signed'])
    df['signed_30day_ind'] = np.where(df['date_signed'] - df['date_start'] <= pd.Timedelta(30,'D'), 1, 0)
    return df

df['signed_30day_ind'].mean() / df['du_3day_ind'].mean()

#4. extrapolate DU wac to signed wac
df = add_du_ind(df)
du_wac = (df['requested_amount'] * df['du_ind'] * df['interest_rate']).sum() / (df['requested_amount'] * df['du_ind']).sum()
signed_wac = (df['requested_amount'] * df['signed_30day_ind'] * df['interest_rate']).sum() / (df['requested_amount'] * df['signed_30day_ind']).sum()
du_wac - signed_wac 

#5. validate on previous dataset
e_ol = Optimization(rate_model_file_lr='../pl_opt/opt_0204/model/elasticity_coef_table_0204.csv', 
                    term_model_file_lr='../pl_opt/opt_0204/new_mnlogit/Term_Selection_Coef_0219.csv',
                    data_file='../pl_opt/opt_0204/data/opt_data_tier_dist_cr.csv', 
                    base_price_grid_file='../erika_git/optimization/test_0219/erika_3_97.xlsx',
                    loss_file='/home/ec2-user/SageMaker/erika_git/optimization/loss_data.csv',
                    save_dir='../erika_git/optimization/test_0219', model_nm='erika_2',
                    desired_wac=0.11915, desired_waloss=0.0675, desired_term_67=0.15,
                    calculate_overlay=True)
df_01_score = e_ol.score_lightgbm(rate_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_elasticity_model.txt', 
                                  term_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_term_model_with_diff.txt', 
                                  rate_header_file='../pl_model_2020_01/lightgbm_model/round2_7c_rate_header.csv', 
                                  term_header_file='../round_2/experiments/7h_2019-11-26_17-16-51/round2_7h_term_header.csv', 
                                  data_file='../pl_opt/opt_0204/data/opt_tier_dist.csv', calculate_overlay=True)
pd.DataFrame(e_ol.cal_basic_metrics(df_01_score)).transpose()

e_ol.raw_data_with_price[['id'] + [f'min_rate_{term}' for term in range(2, 8)]].to_csv('../adhoc/BA_BB/e_ol.csv', index=None)
df = pd.read_csv('../pl_opt/opt_0204/data/opt_tier_dist.csv')
df = pd.merge(df.drop([f'min_rate_{term}' for term in range(2, 8)], axis=1), 
              e_ol.raw_data_with_price[['id'] + [f'min_rate_{term}' for term in range(2, 8)]], on='id')
df.to_csv('../adhoc/BA_BB/e_ol_all.csv', index=None)

term_pred_opt = e_ol.term_score_lightgbm(df,
                                         term_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_term_model_with_diff.txt', 
                                term_header_file='../round_2/experiments/7h_2019-11-26_17-16-51/round2_7h_term_header.csv')

df = score_lightgbm(rate_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_elasticity_model.txt', 
                           term_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_term_model_with_diff.txt', 
                           rate_header_file='../pl_model_2020_01/lightgbm_model/round2_7c_rate_header.csv', 
                           term_header_file='../round_2/experiments/7h_2019-11-26_17-16-51/round2_7h_term_header.csv', 
                           rate_cols=rate_cols,
                           data_file='../adhoc/BA_BB/e_ol_all.csv',
                           output_file='../adhoc/BA_BB/e_ol_lightgbm.csv')
df = eread('../adhoc/BA_BB/e_ol_lightgbm.csv')
term_pred = term_score_lightgbm(df, 
                                term_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_term_model_with_diff.txt', 
                                term_header_file='../round_2/experiments/7h_2019-11-26_17-16-51/round2_7h_term_header.csv')

df = eread('../adhoc/BA_BB/e_ol_lightgbm.csv')
pd.DataFrame(cal_basic_metrics(df)).transpose()
_=compare_dat(df, df_01_score, idvar='id', common_cols=['rate_pred', 'requested_amount'] + [f'pred_term_{term}' for term in range(2, 8)])

df = score_residual(rate_model_file='../pl_opt/opt_0204/model/elasticity_coef_table_0204.csv', 
                    term_model_file='../pl_opt/opt_0204/new_mnlogit/Term_Selection_Coef_0219.csv', 
                    max_rate_impute=0.18983, 
                    residual_rate_model_file='../pl_opt/opt_0204/model_num_tree_tuning/residual_elasticity_model.txt',
                    residual_rate_header_file='../erika_git/optimization/residual_model/header_residual_rate_3vars.csv',
                    data_file='../adhoc/BA_BB/e_ol_all.csv', 
                    output_file='../adhoc/BA_BB/e_ol_all_score.csv')

score_rate_residual(lightgbm_model_file='../pl_opt/opt_0204/model_num_tree_tuning/residual_elasticity_model.txt', 
                    data_file='../adhoc/BA_BB/e_ol_all.csv', 
                    header_file='../erika_git/optimization/residual_model/header_residual_rate_3vars.csv', 
                    output_file='../adhoc/BA_BB/e_ol_score_rate_residual.csv')

score_rate_residual(lightgbm_model_file='../pl_opt/opt_0204/model_num_tree_tuning/residual_elasticity_model.txt', 
                    data_file='../pl_opt/opt_0204/data/opt_rate_pred.csv', 
                    header_file='../erika_git/optimization/residual_model/header_residual_rate_3vars.csv', 
                    output_file='../adhoc/BA_BB/old_residual.csv')

pd.DataFrame(cal_basic_metrics(df)).transpose()

c1 = pd.merge(df[['id', 'rate_pred', 'pred_from_rate', 'residual_pred']], 
              e_ol.score_data[['id', 'rate_pred', 'pred_from_rate', 'residual_pred']], on='id')
for var in ['rate_pred', 'pred_from_rate', 'residual_pred']:
    stat, _ = compare_dat_3(c1, var+'_x', var+'_y')
    print(stat)
    
_=compare_dat(df, e_ol.score_data, idvar='id', common_cols=['rate_pred', 'pred_from_rate', 'residual_pred'])
df2 = eread('../adhoc/BA_BB/e_ol_score_rate_residual.csv')
df3 = eread('../adhoc/BA_BB/old_residual.csv')
_ = compare_dat(df3,e_ol.score_data, idvar='id', common_cols=['residual_pred'])

df4 = eread('../pl_opt/opt_0204/data/opt_rate_pred.csv')
df5 = eread('../adhoc/BA_BB/e_ol_all.csv')
_ = compare_dat(df4, df5, idvar='id', common_cols=['requested_amount'])

df = pd.read_csv('../adhoc/BA_BB/e_ol_all_score.csv')
_=compare_dat(df, e_ol.score_data, idvar='id')