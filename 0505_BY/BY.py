#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  5 10:40:02 2020

@author: esun
"""
import seaborn as sns
from opt_metrics import basic_metrics

#Analyze BY grid

#Compare to simulation data, distribution shift on loan amount and channel in histogram

#1. pull data
#1. query data

df = query_snowflake(qry)
qry = f"""
SELECT
af.id,
af.applicant_id,
af.date_signed,
af.date_doc_upload,
af.date_start,
af.date_fund,
af.initial_term,
af.interest_rate,
af.interest_rate_type,
af.requested_amount,
af.gross_income,
af.app_created_via_mobile,
af.g_program,
af.g_grad_year,
af.credit_score,
af.employer_name,
af.years_of_experience,
af.pl_funds_use,
af.tax_burden_amount,
af.consolidated_channel,
af.attr_affiliate_referrer,
af.campaign_welcome_bonus,
af.housing_status,
acaf.pil0438,
af.registration_date,
af.revolving_credit_amount,
af.ug_ctgry,
af.ug_grad_year,
af.ug_program,
af.member_indicator,
acaf.iln5020,
acaf.alx8220,
acaf.all8020,
acaf.iln5820,
acaf.all0416,
acaf.REV0416,
acaf.mtf5020,
acaf.mta5020,
acaf.mta5830,
acaf.mtj5030,
acaf.mtj5820,
acaf.all5820,
acaf.all7516,
acaf.all8220,
acaf.all5020,
acaf.bcc7110,
acaf.bcc8322,
acaf.bcx3421,
acaf.bcx3422,
acaf.bcx5020,
acaf.bcx5320,
acaf.bcx7110,
acaf.rev5020,
acaf.rev8320,
acaf.rta7300,
acaf.rtr5030,
acaf.all7517,
acaf.iln0416,
acaf.mta0416,
acaf.mta1370,
acaf.mta2800,
acaf.mta8150,
acaf.mta8153,
acaf.mta8157,
acaf.mta8160,
acaf.mtf0416,
acaf.mtf4260,
acaf.mtf8166,
acaf.mts5020,
af.free_cash_flow_pre,
CASE WHEN af.date_doc_upload IS NOT NULL THEN 1 ELSE 0 END AS docs_ind,
CASE WHEN af.date_signed IS NOT NULL THEN 1 ELSE 0 END AS signed_ind,
coalesce(af.tier, oo.tier, ooo.tier) AS tier,
coalesce(oo.sub_tier,ooo.sub_tier) AS sub_tier,
coalesce(oo.champion_challenger_name,ooo.champion_challenger_name) as challenger_name,
oo.rate_2,
oo.rate_3,
oo.rate_4,
oo.rate_5,
oo.rate_6,
oo.rate_7,
coalesce(oo.offer_date,ooo.offer_date) offer_date
FROM dwmart.applications_file af
JOIN dwmart.application_credit_attributes_file acaf ON af.dw_application_id = acaf.dw_application_id
LEFT JOIN                     
    (SELECT
    paf.application_id,
    min(o.champion_challenger_name) as champion_challenger_name,
    min(CASE WHEN p.product_term = 2 THEN ofr.min_rate END) AS rate_2,
    min(CASE WHEN p.product_term = 3 THEN ofr.min_rate END) AS rate_3,
    min(CASE WHEN p.product_term = 4 THEN ofr.min_rate END) AS rate_4,
    min(CASE WHEN p.product_term = 5 THEN ofr.min_rate END) AS rate_5,
    min(CASE WHEN p.product_term = 6 THEN ofr.min_rate END) AS rate_6,
    min(CASE WHEN p.product_term = 7 THEN ofr.min_rate END) AS rate_7,
    min(REGEXP_SUBSTR(min_tier_code,'[1-9]')) AS tier,
    min(o.sub_tier) as sub_tier,
    min(d.CALENDAR_DATE) AS offer_date
    FROM sofidw.product_application_facts paf
    JOIN sofidw.underwriting_info ui ON ui.underwriting_info_id = coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0), NULLIF(paf.initial_uw_id, 0))
    JOIN sofidw.offer_facts ofr ON ofr.underwriting_info_id = ui.underwriting_info_id
    JOIN sofidw.offer_details o on ofr.offer_details_id = o.offer_details_id
    JOIN sofidw.products p ON p.product_id = ofr.product_id
    JOIN sofidw.dates d ON d.DATE_ID = ofr.ASOF_DATE_ID
    JOIN dwmart.applications_file af2 on af2.dw_application_id = paf.application_id AND af2.requested_amount > ofr.min_amount AND af2.requested_amount <= ofr.max_amount
    WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
    GROUP BY paf.APPLICATION_ID
    ) oo ON oo.application_id = af.dw_application_id
LEFT JOIN           
    (SELECT
    paf.application_id,
    min(o.champion_challenger_name) as champion_challenger_name,
    min(REGEXP_SUBSTR(min_tier_code,'[1-9]')) AS tier,
    min(o.sub_tier) as sub_tier,
    min(d.CALENDAR_DATE) AS offer_date
    FROM sofidw.product_application_facts paf
    JOIN sofidw.underwriting_info ui ON ui.underwriting_info_id =
    coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0),
    NULLIF(paf.initial_uw_id, 0))
    JOIN sofidw.offer_facts ofr ON ofr.underwriting_info_id = ui.underwriting_info_id
    JOIN sofidw.offer_details o on ofr.offer_details_id = o.offer_details_id
    JOIN sofidw.products p ON p.product_id = ofr.product_id
    JOIN sofidw.dates d ON d.DATE_ID = ofr.ASOF_DATE_ID
    WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
    group by paf.application_id
    )ooo ON ooo.application_id = af.dw_application_id
WHERE
af.current_decision = 'ACCEPT'
AND af.date_start >= '2018-04-01'
AND af.application_type in ('PL')
"""

#2. clean data
start_date = '2020-04-21'
end_date = '2020-05-05'
df['offer_date'] = pd.to_datetime(df['offer_date'])
df2 = df[(df['offer_date'] >= start_date) & (df['offer_date'] <= end_date)]
df2['challenger_name'].value_counts()
#BY is chase
df3 = df2[df2['challenger_name'] == 'Chase']
df3.to_csv('/Users/esun/Documents/erika_git/adhoc/0505_BY/by.csv', index=None)
#BS is O.G. Champion
df3 = df2[df2['challenger_name'] == 'O.G. Champion']
df3.to_csv('/Users/esun/Documents/erika_git/adhoc/0505_BY/bs.csv', index=None)
#BT is O.G. Challenger
df3 = df2[df2['challenger_name'] == 'O.G. Challenger']
df3.to_csv('/Users/esun/Documents/erika_git/adhoc/0505_BY/bt.csv', index=None)
#BW
df3 = df2[df2['challenger_name'] == 'Morgan']
df3.to_csv('/Users/esun/Documents/erika_git/adhoc/0505_BY/bw.csv', index=None)
#BX
df3 = df2[df2['challenger_name'] == 'Rothschild']
df3.to_csv('/Users/esun/Documents/erika_git/adhoc/0505_BY/bx.csv', index=None)

#3. histogram
#a. requested amount
bs = pd.read_csv('../adhoc/BY/bs_process.csv')
sns.distplot(bs['requested_amount'], hist=True)
sns.distplot(bs[bs['consolidated_channel_model']=='Affiliate']['requested_amount'], hist=True)

bs.groupby('consolidated_channel_model')['requested_amount'].sum() / bs['requested_amount'].sum()
Affiliate           0.306430
MKT-DM              0.017295
Rest                0.239797
combined_organic    0.436478

bs.groupby('consolidated_channel_model')['min_rate_5'].describe().transpose()

df = pd.read_csv('../adhoc/BY/by_process.csv')
sns.distplot(df['requested_amount'], hist=True)

df2 = pd.read_csv('../pl_opt/opt_0408/data/opt_data_md.csv')
sns.distplot(df2['requested_amount'], hist=True)
sns.distplot(df2[df2['consolidated_channel_model']=='Affiliate']['requested_amount'], hist=True)

xinjia = pd.read_csv('../adhoc/BY/opt_data_md.csv')
xinjia.groupby('consolidated_channel_model')['requested_amount'].sum() / xinjia['requested_amount'].sum()
Affiliate           0.290995
MKT-DM              0.085447
Rest                0.176203
combined_organic    0.447355
sns.distplot(df2['requested_amount'], hist=True)

df_score.groupby('consolidated_channel_model')['requested_amount'].sum() / df_score['requested_amount'].sum()
sns.distplot(df_score['requested_amount'], hist=True)
sns.distplot(df['requested_amount'], hist=True)
df_score.groupby('consolidated_channel_model')['min_rate_5'].describe().transpose()

#b. channel
sns.countplot(x="consolidated_channel_model", data=df2, order=['combined_organic', 'Affiliate', 'Rest', 'MKT-DM'])
sns.countplot(x="consolidated_channel_model", data=df, order=['combined_organic', 'Affiliate', 'Rest', 'MKT-DM'])

df = pd.read_csv('../adhoc/BY/by.csv')
channel_list = ['www.sofi.com', 'Affiliate', 'SEM', 'Organic Search', 'MKT-DM', 'P2P']
df = cut_cat_var(df, 'consolidated_channel', channel_list)
df['consolidated_channel_model'] = df['consolidated_channel']
df['consolidated_channel_model'] = np.where(df['consolidated_channel'].isin(['www.sofi.com', 'Organic Search']), 'combined_organic', df['consolidated_channel_model'])
df['consolidated_channel_model'] = np.where(df['consolidated_channel'].isin(['P2P', 'Rest', 'SEM']), 'Rest', df['consolidated_channel_model'])

#4. try to find contribution of wac change
df = pd.read_csv('/home/ec2-user/SageMaker/adhoc/BY/bw.csv')
rate_cols = [f'rate_{term}' for term in range(2, 8)]
df = df[~df[rate_cols].isnull().all(axis=1)]
#df.shape 2310 -> 1820
#2102 -> 1647, bs
#2220 -> 1713, bt

#5. Form model data
df_opt = process_1(df)
df_opt.to_csv('../adhoc/BY/bt_process.csv', index=None)

def process_1(df):
    df.rename(columns={f'rate_{term}': f'min_rate_{term}' for term in range(2, 8)}, inplace=True)
    PL_terms = [2, 3, 4, 5, 6, 7]
    #Member indicator, grad to 0/1 variable
    df['member_indicator'] = df['member_indicator'].map({False: 0, True: 1})
    df['grad'] = np.where(df['g_program'].notnull(), 1, 0)
    #Define date related variables
    df['date_start'] = pd.to_datetime(df['date_start'])
    min_start_date = df['date_start'].min()
    df['date_start_year'] =  df['date_start'].dt.year
    df['date_start_month'] = df['date_start'].dt.month
    df['date_start_day'] = df['date_start'].dt.day
    df['date_start_weekday'] = df['date_start'].dt.weekday
    df['ref_month'] = df['date_start_year'] * 12 + df['date_start_month'] - (min_start_date.year * 12 + min_start_date.month)
    df['ref_day'] = df['date_start'].dt.dayofyear + (df['date_start_year'] - min_start_date.year) * 365
    #Clean affiliate variable
    df['attr_affiliate_referrer'] = clean_str(df['attr_affiliate_referrer'])
    df['attr_affiliate_referrer_new'] = np.where(df['attr_affiliate_referrer'].isin(['www.sofi.com','www.google.com','credit_karma','lending_tree']),
                                                     df['attr_affiliate_referrer'], 'other')
    #Create indicator variables for selected each term or not
    for term in PL_terms:
        df[f'initial_term_{term}'] = np.where((df['initial_term'] == term), 1, 0)
    #Map interest_rate to individual term columns
    df = flat_rate(df, PL_terms)
    #Consolidate tier columns
    df['tier_pre'] = df['tier']
    
    #Cut and group categorical variables
    g_program_list = ['Business - MBA', 'Other', 'Law - JD', 'Engineering/Computer Science - MS or higher',
                                  'Business - MS or MA or higher', 'Social Sciences - MS or MA or higher']
    channel_list = ['www.sofi.com', 'Affiliate', 'SEM', 'Organic Search', 'MKT-DM', 'P2P']
    affiliate_list = ['www.sofi.com', 'www.google.com', '2019_pl_sofi_mail', 'credit_karma', 'lending_tree', 'nerdwallet']
    df = cut_cat_var(df, 'g_program', g_program_list)
    df = cut_cat_var(df, 'consolidated_channel', channel_list)
    df = cut_cat_var(df, 'attr_affiliate_referrer', affiliate_list)
    
    #Clean date variables
    df['date_start'] = pd.to_datetime(df['date_start'])
    df['g_grad_year'] = df['date_start'].dt.year - df['g_grad_year']
    df['ug_grad_year'] = df['date_start'].dt.year - df['ug_grad_year']
    #registration_date: date_start - var
    df['registration_date'] = pd.to_datetime(df['registration_date'])
    df['registration_date'] = (df['date_start'] - df['registration_date']).dt.days
    
    #add rate_diff columns
    for term in range(2, 7):
        df[f'min_rate_diff_{term+1}_{term}'] = df[f'min_rate_{term+1}'] - df[f'min_rate_{term}']
    
    #Consolidated_channel variable
    df['consolidated_channel_model'] = df['consolidated_channel']
    df['consolidated_channel_model'] = np.where(df['consolidated_channel'].isin(['www.sofi.com', 'Organic Search']), 'combined_organic', df['consolidated_channel_model'])
    df['consolidated_channel_model'] = np.where(df['consolidated_channel'].isin(['P2P', 'Rest', 'SEM']), 'Rest', df['consolidated_channel_model'])
    df_channel_dummy = pd.get_dummies(df['consolidated_channel_model'])
    df = pd.concat([df, df_channel_dummy], axis=1)
    
    return df

#6. score
#Score residual
by_score = score_residual(rate_model_file='../pl_opt/opt_0330/model/elasticity_coef_table_0330.csv', 
                        term_model_file='../pl_opt/opt_0330/model/Term_Selection_Coef_0330.csv', 
                        max_rate_impute=0.20329, 
                        residual_rate_model_file='../pl_opt/opt_0330/model/residual_elasticity_model.txt',
                        residual_rate_header_file='../erika_git/optimization/residual_model/header_residual_rate_3vars.csv',
                        data_file='../adhoc/BY/by_process.csv', 
                        output_file='../adhoc/BY/by_score.csv')

basic_metrics(by_score, 'rate_pred', 'avg_rate')

bs_score = score_residual(rate_model_file='../pl_opt/opt_0330/model/elasticity_coef_table_0330.csv', 
                        term_model_file='../pl_opt/opt_0330/model/Term_Selection_Coef_0330.csv', 
                        max_rate_impute=0.20329, 
                        residual_rate_model_file='../pl_opt/opt_0330/model/residual_elasticity_model.txt',
                        residual_rate_header_file='../erika_git/optimization/residual_model/header_residual_rate_3vars.csv',
                        data_file='../adhoc/BY/bs_process.csv', 
                        output_file='../adhoc/BY/bs_score.csv')

basic_metrics(bs_score, 'rate_pred', 'avg_rate')

bt_score = score_residual(rate_model_file='../pl_opt/opt_0330/model/elasticity_coef_table_0330.csv', 
                        term_model_file='../pl_opt/opt_0330/model/Term_Selection_Coef_0330.csv', 
                        max_rate_impute=0.20329, 
                        residual_rate_model_file='../pl_opt/opt_0330/model/residual_elasticity_model.txt',
                        residual_rate_header_file='../erika_git/optimization/residual_model/header_residual_rate_3vars.csv',
                        data_file='../adhoc/BY/bt_process.csv', 
                        output_file='../adhoc/BY/bt_score.csv')

basic_metrics(bt_score, 'rate_pred', 'avg_rate')

#6b. check on bs simulation
bs_score = score_residual(rate_model_file='../pl_opt/opt_0330/model/elasticity_coef_table_0330.csv', 
                        term_model_file='../pl_opt/opt_0330/model/Term_Selection_Coef_0330.csv', 
                        max_rate_impute=0.20329, 
                        residual_rate_model_file='../pl_opt/opt_0330/model/residual_elasticity_model.txt',
                        residual_rate_header_file='../erika_git/optimization/residual_model/header_residual_rate_3vars.csv',
                        data_file='../adhoc/BY/bs_process.csv', 
                        output_file='../adhoc/BY/bs_score.csv')


df = pd.read_csv('../adhoc/BY/BS_0505_Data.csv')
rate_model_file='../pl_opt/opt_0330/model/elasticity_coef_table_0330.csv'
term_model_file='../pl_opt/opt_0330/model/Term_Selection_Coef_0330.csv'
max_rate_impute=0.20329
rate_coef = pd.read_csv(rate_model_file)
term_coef = pd.read_csv(term_model_file)
term_coef.set_index('Variable', inplace=True) 
df.rename(columns={f'min_rate_{term}_new': f'min_rate_{term}' for term in range(2, 8)}, inplace=True)

df = df.reset_index()
df['id'] = df['index']

df_score = score(df, rate_coef, term_coef, max_rate_impute, mask_cols=[f'mask_{term}' for term in range(2, 8)])
basic_metrics(df_score, 'rate_pred', 'avg_rate')



#7. Simulate
by = pd.read_csv('../adhoc/BY/by_process.csv')
by = create_fico_loan_income_bin(by)

#random number 1 - 100 to every obs
#by['rand'] = np.random.randint(1, 100, by.shape[0])
#by[by['rand']< 60].shape[0] / by.shape[0]
#by = by[~((by['requested_amount'] <=20000) & (by['rand'] >= 62.4))]
#by_more = by[(by['loan_amt_bin']=='(20000, 50000]') & (by['rand'] < 15)]

by['requested_amount_new'] = np.where(by['loan_amt_bin'] == '[5000, 20000]',
                                      by['requested_amount'] * 0.62,
                                      np.where(by['loan_amt_bin'] == '(20000, 50000]',
                                      by['requested_amount'] * 1.15,
                                      by['requested_amount']))

by.groupby('loan_amt_bin')['requested_amount'].sum() / by['requested_amount'].sum()
by.groupby('loan_amt_bin')['requested_amount_new'].sum() / by['requested_amount_new'].sum()

by_score = score_residual(rate_model_file='../pl_opt/opt_0330/model/elasticity_coef_table_0330.csv', 
                        term_model_file='../pl_opt/opt_0330/model/Term_Selection_Coef_0330.csv', 
                        max_rate_impute=0.20329, 
                        residual_rate_model_file='../pl_opt/opt_0330/model/residual_elasticity_model.txt',
                        residual_rate_header_file='../erika_git/optimization/residual_model/header_residual_rate_3vars.csv',
                        data_file='../adhoc/BY/by_process.csv', 
                        output_file='../adhoc/BY/by_score.csv')

by_score = pd.merge(by_score, by[['id', 'requested_amount_new']], on='id')
basic_metrics(by_score, 'rate_pred', 'avg_rate', loan_amt_col='requested_amount_new')
by_score = create_fico_loan_income_bin(by_score, income=False)
by_score.groupby('loan_amt_bin')['rate_pred'].mean()

