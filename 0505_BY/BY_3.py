#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  7 10:08:11 2020

@author: esun
"""

#Understand wac difference on 20 - 50 k bucket
by = pd.read_csv('../adhoc/BY/by_score.csv')
by_orig = pd.read_csv('../adhoc/BY/by_process.csv')
#by_orig['signed_ind'] = np.where(by_orig['date_signed'].notnull(), 1, 0)
by_orig = add_signed_ind(by_orig, terms=range(2, 8), remove_mix_labels=False)
by = pd.merge(by, by_orig[['id', 'signed_ind', 'tier', 'sub_tier', 'interest_rate'] + [f'signed_ind_{term}' for term in range(2, 8)]], on='id')
by = by[(by['requested_amount'] > 20000) & (by['requested_amount'] <= 50000)]
by['sub_tier'] = by['sub_tier'].fillna('a')
by['tier'] = by['tier'].astype(str)
by['new_tier'] = by['tier'] + by['sub_tier']
by.groupby('new_tier')['requested_amount'].sum() / by['requested_amount'].sum()

df = by
var = 'new_tier'
bins = ['1a', '1b', '2a', '2b', '3a', '3b', '4a', '4b', '5a']
df_result = pd.DataFrame()
for value in bins:
    df_iter = df[df[var] == value]
    perf_iter = pd.DataFrame(list(basic_metrics(df_iter, 'rate_pred', 'avg_rate'))).transpose()
    perf_iter.index = [value]
    df_result = pd.concat([df_result, perf_iter], axis=0)
    
for value in bins:
    df_iter = df[df[var] == value]
    perf_iter = pd.DataFrame(list(basic_metrics(df_iter, 'signed_ind', 'interest_rate'))).transpose()
    perf_iter.index = [value]
    df_result = pd.concat([df_result, perf_iter], axis=0)
    
t5 = OptMD5(rate_model_file_lr='../pl_opt/opt_0330/model/elasticity_coef_table_0330.csv', 
                term_model_file_lr='../pl_opt/opt_0330/model/Term_Selection_Coef_0330.csv', 
                data_file='../pl_opt/opt_0408/data/opt_data_md.csv',
                loss_file='../pl_opt/opt_0408/data/loss_data_new_tier_severe.csv',
                desired_wac=0.1168, desired_waloss=0.0535, desired_term_67=1,
                base_price_grid_file='../pl_opt/opt_0408/test_0417c/pg_c_470_headline_rate.csv', 
                tier_col='new_tier', tier_no=9,
                save_dir='../pl_opt/opt_0408/test_0417c', model_nm='0417_c',
                max_rates_1=[0.13588, 0.15499, 0.15749, 0.16249, 0.16736, 0.18121],
                max_rates_2=[0.1694, 0.17235, 0.17575, 0.1809])

df = t5.score_data
df = df[(df['requested_amount'] > 20000) & (df['requested_amount'] <= 50000)]
df_result = pd.DataFrame()
bins = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
df_result = pd.DataFrame()
for value in bins:
    df_iter = df[df[var] == value]
    perf_iter = pd.DataFrame(list(basic_metrics(df_iter, 'rate_pred', 'avg_rate'))).transpose()
    perf_iter.index = [value]
    df_result = pd.concat([df_result, perf_iter], axis=0)
    
df.groupby('new_tier')['requested_amount'].sum() / df['requested_amount'].sum()
loan_amt_col='requested_amount'
tier_col='new_tier'
#sign_col='rate_pred'
#rate_col='avg_rate'
#term_prob_cols=[f'pred_term_{term}' for term in range(2, 8)]
sign_col='signed_ind'
rate_col='interest_rate'
term_prob_cols=[f'signed_ind_{term}' for term in range(2, 8)]


df['signed_loan_amt'] = df[loan_amt_col] * df[sign_col]
signed_loan_amt_by_term = pd.DataFrame(np.expand_dims(df['signed_loan_amt'].values, axis=1) * df[term_prob_cols].values)
signed_loan_amt_term_cols = [f'signed_loan_amt_{term}' for term in range(2,8)]
signed_loan_amt_by_term.columns = signed_loan_amt_term_cols

df2 = pd.concat([df.reset_index(drop=True), signed_loan_amt_by_term.reset_index(drop=True)], axis=1)
result = df2.groupby(tier_col)[signed_loan_amt_term_cols].sum() / df2['signed_loan_amt'].sum()
