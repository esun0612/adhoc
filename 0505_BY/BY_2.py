#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  6 09:32:51 2020

@author: esun
"""
import pandas as pd
import sys
sys.path.append('../erika_git/optimization/opt_utils')
from opt_util_data import create_fico_loan_income_bin

#tier and loan amount distribution
df_opt = pd.read_csv('../pl_opt/opt_0408/data/opt_data_md.csv')
stat = df_opt.groupby(['new_tier', 'loan_amt_bin'])['requested_amount'].sum() / df_opt['requested_amount'].sum()
stat.unstack('loan_amt_bin')

by = pd.read_csv('../adhoc/BY/by_process.csv')
by['signed_ind'] = np.where(by['date_signed'].notnull(), 1, 0)
by = create_fico_loan_income_bin(by)
by['sub_tier'] = by['sub_tier'].fillna('a')
by['tier'] = by['tier'].astype(str)
by['new_tier'] = by['tier'] + by['sub_tier']
(by.groupby(['new_tier', 'loan_amt_bin'])['requested_amount'].sum() / by['requested_amount'].sum()).unstack('loan_amt_bin')
by.groupby('loan_amt_bin')['requested_amount'].sum() / by['requested_amount'].sum()
by.groupby('loan_amt_bin')['signed_ind'].sum() / by['signed_ind'].sum()

by_score = score_residual(rate_model_file='../pl_opt/opt_0330/model/elasticity_coef_table_0330.csv', 
                        term_model_file='../pl_opt/opt_0330/model/Term_Selection_Coef_0330.csv', 
                        max_rate_impute=0.20329, 
                        residual_rate_model_file='../pl_opt/opt_0330/model/residual_elasticity_model.txt',
                        residual_rate_header_file='../erika_git/optimization/residual_model/header_residual_rate_3vars.csv',
                        data_file='../adhoc/BY/by_process.csv', 
                        output_file='../adhoc/BY/by_score.csv')

bt = pd.read_csv('../adhoc/BY/bt_process.csv')
bt = create_fico_loan_income_bin(bt)
bt['sub_tier'] = bt['sub_tier'].fillna('a1')
bt['tier'] = bt['tier'].astype(str)
bt['new_tier'] = bt['tier'] + bt['sub_tier']
(bt.groupby(['new_tier', 'loan_amt_bin'])['requested_amount'].sum() / bt['requested_amount'].sum()).unstack('loan_amt_bin')

#Karan dist and wac
by = pd.read_csv('../adhoc/BY/by_process.csv')
by = create_fico_loan_income_bin(by)
by.groupby('loan_amt_bin')['requested_amount'].sum() / by['requested_amount'].sum()
by.groupby('consolidated_channel_model')['requested_amount'].sum() / by['requested_amount'].sum()


optdf = pd.read_csv('../pl_opt/opt_0408/data/opt_data_md.csv')
optdf.groupby('loan_amt_bin')['requested_amount'].sum() / optdf['requested_amount'].sum()
optdf.groupby('consolidated_channel_model')['requested_amount'].sum() / optdf['requested_amount'].sum()

df = by_score
df = create_fico_loan_income_bin(df, income=False)
var = 'loan_amt_bin'
bins = ['[5000, 20000]', '(20000, 50000]', '(50000, 100000]']
df_result = pd.DataFrame()
for value in bins:
    df_iter = df[df[var] == value]
    perf_iter = pd.DataFrame(list(basic_metrics(df_iter, 'rate_pred', 'avg_rate'))).transpose()
    perf_iter.index = [value]
    df_result = pd.concat([df_result, perf_iter], axis=0)
    
df = t5.score_data
df = create_fico_loan_income_bin(df, income=False)
var = 'consolidated_channel_model'
bins = ['Affiliate', 'MKT-DM', 'Rest', 'combined_organic']
df_result = pd.DataFrame()
for value in bins:
    df_iter = df[df[var] == value]
    perf_iter = pd.DataFrame(list(basic_metrics(df_iter, 'rate_pred', 'avg_rate'))).transpose()
    perf_iter.index = [value]
    df_result = pd.concat([df_result, perf_iter], axis=0)
    
#actual
df = by
df = create_fico_loan_income_bin(df, income=False)
var = 'loan_amt_bin'
bins = loan_amt_bins = ['[5000, 20000]', '(20000, 50000]', '(50000, 100000]']
df_result = pd.DataFrame()
for value in bins:
    df_iter = df[df[var] == value]
    perf_iter = pd.DataFrame(list(basic_metrics(df_iter, 'signed_ind', 'interest_rate'))).transpose()
    perf_iter.index = [value]
    df_result = pd.concat([df_result, perf_iter], axis=0)

#actual
df = by
df = create_fico_loan_income_bin(df, income=False)
var = 'consolidated_channel_model'
bins = ['Affiliate', 'MKT-DM', 'Rest', 'combined_organic']
df_result = pd.DataFrame()
for value in bins:
    df_iter = df[df[var] == value]
    perf_iter = pd.DataFrame(list(basic_metrics(df_iter, 'signed_ind', 'interest_rate'))).transpose()
    perf_iter.index = [value]
    df_result = pd.concat([df_result, perf_iter], axis=0)
    
#opt wac
t5 = OptMD5(rate_model_file_lr='../pl_opt/opt_0330/model/elasticity_coef_table_0330.csv', 
                term_model_file_lr='../pl_opt/opt_0330/model/Term_Selection_Coef_0330.csv', 
                data_file='../pl_opt/opt_0408/data/opt_data_md.csv',
                loss_file='../pl_opt/opt_0408/data/loss_data_new_tier_severe.csv',
                desired_wac=0.1168, desired_waloss=0.0535, desired_term_67=1,
                base_price_grid_file='../pl_opt/opt_0408/test_0417c/pg_c_470_headline_rate.csv', 
                tier_col='new_tier', tier_no=9,
                save_dir='../pl_opt/opt_0408/test_0417c', model_nm='0417_c',
                max_rates_1=[0.13588, 0.15499, 0.15749, 0.16249, 0.16736, 0.18121],
                max_rates_2=[0.1694, 0.17235, 0.17575, 0.1809])

df = t5.score_data
df_result = pd.DataFrame()
for value in bins:
    df_iter = df[df[var] == value]
    perf_iter = pd.DataFrame(list(basic_metrics(df_iter, 'rate_pred', 'avg_rate'))).transpose()
    perf_iter.index = [value]
    df_result = pd.concat([df_result, perf_iter], axis=0)
    
df = t5.score_data
df['loan_amt_bin'] = pd.cut(df['requested_amount'], [-np.inf, 20000, 30000, 40000, 50000, np.inf])
df.groupby('loan_amt_bin')['requested_amount'].sum() / df['requested_amount'].sum()