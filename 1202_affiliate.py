#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  2 17:03:02 2019

@author: esun

#After 11/15, AL get more affiliate, check distribution
"""
import numpy as np
from util_data import query_snowflake
from util_data import clean_str

dat_dir = '/home/ec2-user/SageMaker/adhoc'
df = query_snowflake(qry)
esave(df, pjoin(dat_dir, '1202.feather'))
df.shape
#Out[86]: (671493, 42)
#snowflake in 64.82058119773865[s]
df['offer_date'] = pd.to_datetime(df['offer_date'])
df = df[df['offer_date'] >= '2019-11-15']
df['challenger_name'].value_counts()
O.G. Champion      2886 -> AL
O.G. Challenger    2054 -> AR
Rothschild         2036 -> AT
Morgan             2034 -> AS
Chase              1975 -> AQ

def cut_cat_var(df, cat_var, cat_var_value_list):
    """For a given cat_var in a DataFrame df, keep values in cat_var_value_list and group other values in 'Rest'
    """
    df[cat_var] = np.where((df[cat_var].isin(cat_var_value_list)) | (df[cat_var].isnull()), df[cat_var], 'Rest')
    return df

df['attr_affiliate_referrer'] = clean_str(df['attr_affiliate_referrer'])
affiliate_list = ['www.sofi.com', 'www.google.com', '2019_pl_sofi_mail', 'credit_karma', 'lending_tree', 'nerdwallet']
df = cut_cat_var(df, 'attr_affiliate_referrer', affiliate_list)

stat = pd.DataFrame(df.groupby(['challenger_name', 'attr_affiliate_referrer'])['id'].count())
stat.reset_index(inplace=True)
stat_pivot = stat.pivot(columns='attr_affiliate_referrer', index='challenger_name')
stat_pivot.to_csv(pjoin(dat_dir, 'analysis.csv'))

qry = """SELECT
 CASE WHEN af.date_start IS NOT NULL THEN 1 ELSE 0 END AS quality_start
,CASE WHEN af.date_submit IS NOT NULL OR af.date_doc_upload IS NOT NULL THEN 1 ELSE 0 END submit
,CASE WHEN af.date_doc_upload IS NOT NULL OR af.date_signed is not null THEN 1 ELSE 0 END AS doc_upload
,CASE WHEN af.date_signed IS NOT NULL OR af.date_fund is not null THEN 1 ELSE 0 END AS signed
,CASE WHEN af.date_fund IS NOT NULL THEN 1 ELSE 0 END AS fund
,af.requested_amount
,af.loan_amt
,af.consolidated_channel
,CASE WHEN af.coborrower_applicant_id IS NOT NULL THEN 1 ELSE 0 END AS coborrower_ind
,af.attr_affiliate_referrer
,af.date_start
,af.pl_funds_use
,af.date_submit
,af.date_doc_upload
,af.date_fund
,af.date_signed
,af.date_signing
,CASE WHEN af.application_type IN ('REFI', 'PLUS') THEN 'REFI' else af.application_type END AS application_type
,af.gross_income
,CASE WHEN af.fico BETWEEN 350 AND 850 THEN af.fico ELSE NULL END AS credit_score
,af.id
,af.pl_custom_score
,af.applicant_id
,af.g_program
,af.age
,CAST(af.initial_term AS INTEGER) as term_selected
,af.credit_score/1000 as credit_score_decimal 
,af.initial_term/100 as term_decimal
,af.interest_rate as rate_selected
,af.interest_rate_type as rate_type_selected
,coalesce(af.tier,oo.tier,ooo.tier) AS tier
,coalesce(oo.champion_challenger_name,ooo.champion_challenger_name) as challenger_name
,coalesce(oo.rate_2,ooo.rate_2) rate_2
,coalesce(oo.rate_3,ooo.rate_3) rate_3
,coalesce(oo.rate_4,ooo.rate_4) rate_4
,coalesce(oo.rate_5,ooo.rate_5) rate_5
,coalesce(oo.rate_6,ooo.rate_6) rate_6
,coalesce(oo.rate_7,ooo.rate_7) rate_7
,coalesce(oo.rate_10,ooo.rate_10) rate_10
,coalesce(oo.rate_15,ooo.rate_15) rate_15
,coalesce(oo.rate_20,ooo.rate_20) rate_20
,coalesce(oo.offer_date,ooo.offer_date) offer_date
FROM dwmart.applications_file af
JOIN sofidw.product_application_facts paf ON paf.application_id = af.dw_application_id
JOIN sofidw.products p ON p.product_id = paf.product_id
LEFT JOIN                     (
                    SELECT
                    paf.application_id,
                    min(o.champion_challenger_name) as champion_challenger_name,
                    min(CASE WHEN p.product_term = 2
                    THEN ofr.min_rate END) AS rate_2,
                    min(CASE WHEN p.product_term = 3
                    THEN ofr.min_rate END) AS rate_3,
                    min(CASE WHEN p.product_term = 4
                    THEN ofr.min_rate END) AS rate_4,
                    min(CASE WHEN p.product_term = 5
                    THEN ofr.min_rate END) AS rate_5,
                    min(CASE WHEN p.product_term = 6
                    THEN ofr.min_rate END) AS rate_6,
                    min(CASE WHEN p.product_term = 7
                    THEN ofr.min_rate END) AS rate_7,
                    min(CASE WHEN p.product_term = 10
                    THEN ofr.min_rate END) AS rate_10,
                    min(CASE WHEN p.product_term = 15
                    THEN ofr.min_rate END) AS rate_15,
                    min(CASE WHEN p.product_term = 20
                    THEN ofr.min_rate END) AS rate_20,
                    min(REGEXP_SUBSTR(min_tier_code,'[1-9]')) AS tier,
                    min(d.CALENDAR_DATE) AS offer_date
                    FROM sofidw.product_application_facts paf
                    JOIN sofidw.underwriting_info ui ON ui.underwriting_info_id =
                    coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0),
                    NULLIF(paf.initial_uw_id, 0))
                    JOIN sofidw.offer_facts ofr ON ofr.underwriting_info_id = ui.underwriting_info_id
                    JOIN sofidw.offer_details o on ofr.offer_details_id = o.offer_details_id
                    JOIN sofidw.products p ON p.product_id = ofr.product_id
                    JOIN sofidw.dates d ON d.DATE_ID = ofr.ASOF_DATE_ID
                    JOIN dwmart.applications_file af2 on af2.dw_application_id = paf.application_id AND af2.requested_amount > ofr.min_amount AND af2.requested_amount <= ofr.max_amount
                    WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
                    GROUP BY paf.APPLICATION_ID
                    ) oo ON oo.application_id = af.dw_application_id
LEFT JOIN           (SELECT
                    paf.application_id,
                    min(o.champion_challenger_name) as champion_challenger_name,
                    min(CASE WHEN p.product_term = 2
                    THEN ofr.min_rate END) AS rate_2,
                    min(CASE WHEN p.product_term = 3
                    THEN ofr.min_rate END) AS rate_3,
                    min(CASE WHEN p.product_term = 4
                    THEN ofr.min_rate END) AS rate_4,
                    min(CASE WHEN p.product_term = 5
                    THEN ofr.min_rate END) AS rate_5,
                    min(CASE WHEN p.product_term = 6
                    THEN ofr.min_rate END) AS rate_6,
                    min(CASE WHEN p.product_term = 7
                    THEN ofr.min_rate END) AS rate_7,
                    min(CASE WHEN p.product_term = 10
                    THEN ofr.min_rate END) AS rate_10,
                    min(CASE WHEN p.product_term = 15
                    THEN ofr.min_rate END) AS rate_15,
                    min(CASE WHEN p.product_term = 20
                    THEN ofr.min_rate END) AS rate_20,
                    min(REGEXP_SUBSTR(min_tier_code,'[1-9]')) AS tier,
                    min(d.CALENDAR_DATE) AS offer_date
                    FROM sofidw.product_application_facts paf
                    JOIN sofidw.underwriting_info ui ON ui.underwriting_info_id =
                    coalesce(NULLIF(paf.final_uw_id, 0), NULLIF(paf.selected_uw_id, 0),
                    NULLIF(paf.initial_uw_id, 0))
                    JOIN sofidw.offer_facts ofr ON ofr.underwriting_info_id = ui.underwriting_info_id
                    JOIN sofidw.offer_details o on ofr.offer_details_id = o.offer_details_id
                    JOIN sofidw.products p ON p.product_id = ofr.product_id
                    JOIN sofidw.dates d ON d.DATE_ID = ofr.ASOF_DATE_ID
                    WHERE o.rate_type_code = 'FIXED' AND o.eligible = TRUE
                    group by paf.application_id
                    ) ooo ON ooo.application_id = af.dw_application_id
WHERE
af.current_decision = 'ACCEPT'
AND af.date_start >= '2018-04-01'
AND af.application_type in ('PL')
"""