#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  3 14:31:41 2020

@author: esun
"""
#opt class
self = Optimization(rate_model_file_lr='../pl_opt/opt_0204/model/elasticity_coef_table_0204.csv', 
                    term_model_file_lr='../pl_opt/opt_0204/new_mnlogit/Term_Selection_Coef_0219.csv',
                    data_file='../pl_opt/opt_0204/data/opt_data_tier_dist_cr.csv', 
                    base_price_grid_file='../erika_git/optimization/test_0219/erika_3_97.xlsx',
                    loss_file='/home/ec2-user/SageMaker/erika_git/optimization/loss_data.csv',
                    save_dir='../erika_git/optimization/test_0219', model_nm='erika_2',
                    desired_wac=0.11915, desired_waloss=0.0675, desired_term_67=0.15,
                    calculate_overlay=True)
df_01_score = self.score_lightgbm(rate_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_elasticity_model.txt', 
                                  term_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_term_model_with_diff.txt', 
                                  rate_header_file='../pl_model_2020_01/lightgbm_model/round2_7c_rate_header.csv', 
                                  term_header_file='../round_2/experiments/7h_2019-11-26_17-16-51/round2_7h_term_header.csv', 
                                  data_file='../pl_opt/opt_0204/data/opt_tier_dist.csv', calculate_overlay=True)

#functions
df = pd.read_csv('../adhoc/BA_BB/e_ol_all.csv')
_=compare_dat(df_01_score, df, idvar='id') #match

df = score_lightgbm(rate_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_elasticity_model.txt', 
                    term_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_term_model_with_diff.txt', 
                    rate_header_file='../pl_model_2020_01/lightgbm_model/round2_7c_rate_header.csv', 
                    term_header_file='../round_2/experiments/7h_2019-11-26_17-16-51/round2_7h_term_header.csv', 
                    rate_cols=rate_cols,
                    data_file='../adhoc/BA_BB/e_ol_all.csv',
                    output_file='../adhoc/BA_BB/e_ol_lightgbm.csv')

df = score_lightgbm(rate_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_elasticity_model.txt', 
                    term_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_term_model_with_diff.txt', 
                    rate_header_file='../pl_model_2020_01/lightgbm_model/round2_7c_rate_header.csv', 
                    term_header_file='../round_2/experiments/7h_2019-11-26_17-16-51/round2_7h_term_header.csv', 
                    rate_cols=rate_cols,
                    data_file='../adhoc/BA_BB/e_ol_all_v2.csv',
                    output_file='../adhoc/BA_BB/e_ol_lightgbm_v2.csv')

df = pd.read_csv('../adhoc/BA_BB/e_ol_lightgbm.csv')
df2 = pd.read_csv('../adhoc/BA_BB/e_ol_lightgbm_v2.csv')
c = compare_dat(df, df2, idvar='id')
c = compare_dat(df_01_score, df, idvar='id')
df = pd.merge(df, df_01_score, on='id')
for rate_col in rate_cols_orig:
    stat, fig = compare_dat_3(df, rate_col+'_x', rate_col+'_y')
    print(stat)
    
c.loc[(c['pred_term_5_x'] - c['pred_term_5_y']).abs()>1e-3, 
     ['id'] + [f'pred_term_{term}_x' for term in range(2, 8)] + [f'pred_term_{term}_y' for term in range(2, 8)]].head()

#Functions step by step
rate_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_elasticity_model.txt'
term_model_file='../pl_opt/opt_0204/lightgbm/lightgbm_term_model_with_diff.txt'
rate_header_file='../pl_model_2020_01/lightgbm_model/round2_7c_rate_header.csv'
term_header_file='../round_2/experiments/7h_2019-11-26_17-16-51/round2_7h_term_header.csv'
rate_cols=rate_cols
data_file='../adhoc/BA_BB/e_ol_all.csv'
output_file='../adhoc/BA_BB/e_ol_lightgbm_step_by_step.csv'

df = pd.read_csv(data_file)
#Process data
impute_rate_dict = {'min_rate_2': 0.1724,
                     'min_rate_3': 0.17435,
                     'min_rate_4': 0.17775,
                     'min_rate_5': 0.18983,
                     'min_rate_6': 0.15313,
                     'min_rate_7': 0.16115}
for term, rate_col in zip(range(2, 8), rate_cols):
    df[f'min_rate_{term}_orig'] = df[rate_col]
    df[f'min_rate_{term}_2'] = df[rate_col].fillna(impute_rate_dict[f'min_rate_{term}'])
df[rate_cols] = df[rate_cols].fillna(value=impute_rate_dict)

rate_cols = [f'min_rate_{term}' for term in range(2, 8)]
rate_cols_orig = [f'min_rate_{term}_orig' for term in range(2, 8)]
term_prob_cols = [f'pred_term_{term}' for term in range(2, 8)]
df['min_all_rates'] = df[rate_cols].min(axis=1)
for term in range(2, 7):
    df[f'min_rate_diff_{term+1}_{term}'] = df[f'min_rate_{term+1}'] - df[f'min_rate_{term}']

#Score rate model
rate_pred = rate_score_lightgbm(df, rate_model_file, rate_header_file)
#Score term model
term_pred = term_score_lightgbm(df, term_model_file, term_header_file)


#%%Validate again on raw score -> match
self = e_ol
#Score rate model
rate_pred_opt = self.rate_score_lightgbm(df, rate_model_file, rate_header_file)
#Score term model
term_pred_opt = self.term_score_lightgbm(df, term_model_file, term_header_file)
c1 = compare_dat(term_pred, term_pred_opt, idvar='id')

#%%Rate adjustment works on mnlogit model?

#a. Functions
final_score = pd.merge(rate_pred, term_pred, on='id')
selected_cols = ['id', 'tier_pre', 'requested_amount', 'credit_score'] + ['combined_organic', 'MKT-DM', 'Rest', 'SEM'] + \
                rate_cols + rate_cols_orig 
final_score = pd.merge(final_score, df[selected_cols], on='id')
#Adjust term score to sum to 1
final_score = adjust_term_rate(final_score, rate_cols_orig, term_prob_cols)
final_score = cal_avg(final_score, range(2, 8), rate_cols, term_prob_cols)
final_score = cal_N_D(final_score)

#b. class
for loss_col in self.loss_cols:
    df[loss_col] = 0
final_score_opt = pd.merge(rate_pred_opt, term_pred_opt, on='id')
selected_cols = ['id', 'tier_pre', 'requested_amount', 'credit_score'] + ['combined_organic', 'MKT-DM', 'Rest', 'SEM'] + \
                self.rate_cols + rate_cols_orig + self.loss_cols
final_score_opt = pd.merge(final_score_opt, df[selected_cols], on='id')
#Adjust term score to sum to 1
final_score_opt = self.adjust_term_rate(final_score_opt, rate_cols_orig)

        
c2 = compare_dat(final_score, self.cal_avg(final_score_opt), idvar='id')
c3 = compare_dat(final_score, df, idvar='id')
c3 = compare_dat(self.cal_avg(final_score_opt), df_01_score, idvar='id')
c4 = compare_dat(df_01_score, term_pred, idvar='id')
c5 = compare_dat(term_pred_opt_2, term_pred, idvar='id')
c6= compare_dat(df_01_score, df, idvar='id')
df_01_score.to_csv('../adhoc/BA_BB/e_ol_all_v2.csv', index=None)
term_pred_opt_2 = self.term_score_lightgbm(df, term_model_file, term_header_file)