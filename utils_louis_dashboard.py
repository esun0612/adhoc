#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 13:12:52 2020

@author: esun

Utility functions for Louis to build the model tracking dashboard
"""
import re
import numpy as np
import time
import pandas as pd
import lightgbm as lgb
from os.path import splitext
#import snowflake.connector

def eread(data_file):
    """Utility function to have same interface for reading csv and feather data
    """
    data_file_ext = splitext(data_file)[-1]
    if data_file_ext == ".csv":
        df = pd.read_csv(data_file)
    elif data_file_ext == ".feather":
        df = pd.read_feather(data_file)
    return df

def esave(df, data_file):
    """Utility function to have same interface for saving csv and feather data
    """
    data_file_ext = splitext(data_file)[-1]
    if data_file_ext == ".csv":
        df.to_csv(data_file, index=None)
    elif data_file_ext == ".feather":
        try:
            df.to_feather(data_file)
        except ValueError:
            df.reset_index(drop=True).to_feather(data_file)
            
def score_residual(rate_model_file, term_model_file, max_rate_impute, 
                   residual_rate_model_file, residual_rate_header_file,
                   data_file, output_file):
    """Wrapper function to score residual model
    """
    df = pd.read_csv(data_file)
    df = data_process_residual(df)
    rate_coef = pd.read_csv(rate_model_file)
    term_coef = pd.read_csv(term_model_file)
    term_coef.set_index('Variable', inplace=True) 
    df_rate_residual = rate_score_lightgbm(df, residual_rate_model_file, residual_rate_header_file)
    df_rate_residual.rename(columns={'rate_pred': 'residual_pred'}, inplace=True)
    df = pd.merge(df, df_rate_residual, on='id')
    df_score = score(df, rate_coef, term_coef, max_rate_impute)
    df_score.to_csv(output_file, index=None)
    return df_score
      
def data_process_residual(df):
    """Create bucket indicator columns
    """
    df['requested_amount_less_than_20000'] = np.where(df['requested_amount'] < 20000, 1, 0)
    df['requested_amount_more_equal_50000'] = np.where(df['requested_amount'] >= 50000, 1, 0)
    credit_score_bins= [680, 700, 720, 740, 760, 780, 800]
    df['fico_bin'] = pd.cut(df['credit_score'], bins=credit_score_bins, include_lowest=True, right=False).astype(str) 
    fico_dummy = pd.get_dummies(df['fico_bin']).iloc[:, :-1]
    cr_score_bin_name = ['less_than_700'] + [f'{lb}_{ub}' for lb, ub in zip(range(700, 800, 20), range(720, 820, 20))]
    fico_dummy.columns = [f'credit_score_{bin}' for bin in cr_score_bin_name]
    df = pd.merge(df, fico_dummy, left_index=True, right_index=True)
    return df

def score(df, rate_coef, term_coef, max_rate_impute, 
          id_col='id', terms=range(2, 8), rate_cols=[f'min_rate_{term}' for term in range(2, 8)],
          mask_cols=None):
    """Score current price grid
    """
    rate_pred = rate_score(df, rate_coef, terms=terms, rate_cols=rate_cols) 
    term_pred = term_score(df, term_coef, max_rate_impute, terms=terms, rate_cols=rate_cols, mask_cols=mask_cols)
    final_score = pd.merge(rate_pred, term_pred, on=id_col)
    return cal_avg(final_score, terms, rate_cols, [f'pred_term_{term}' for term in range(2, 8)])

class Header():
    """Helper class for header file
    """
    def __init__(self, header_file=None, header=None):
        if header_file is not None:
            self.header = pd.read_csv(header_file)
        elif header is not None:
            self.header = header
        else:
            "Please input header dataframe or header_file"
            return 
        self.cols = list(self.header.columns)
        self.types = list(self.header.loc[0])
        self.index = list(range(0, len(self.cols)))
        self.num_cols = [x for x, y in zip(self.cols, self.types) if y == 'N']
        self.num_cols_idx = [x for x, y in zip(self.index, self.types) if y == 'N']
        self.cat_cols = [x for x, y in zip(self.cols, self.types) if y == 'C']
        self.cat_cols_idx = [x for x, y in zip(self.index, self.types) if y == 'C']
        self.key_cols = [x for x, y in zip(self.cols, self.types) if y == 'K']
        self.key_cols_idx = [x for x, y in zip(self.index, self.types) if y == 'K']
        self.label_cols = [x for x, y in zip(self.cols, self.types) if y == 'L']
        self.label_cols_idx = [x for x, y in zip(self.index, self.types) if y == 'L']
        self.ignore_cols = [x for x, y in zip(self.cols, self.types) if y == 'I']
        self.ignore_cols_idx = [x for x, y in zip(self.index, self.types) if y == 'I']
        
def object2cat(X, use_cat_cols=None, return_cat_cols=False):
    """Cast categorical columns to type 'category'
    """
    if use_cat_cols is None:
        cat_cols = []
        for c in X.columns:
            col_type = X[c].dtype
            if col_type == 'object' or col_type.name == 'category':
                X[c] = X[c].astype('category')
                cat_cols.append(c)
    else:
        cat_cols = use_cat_cols
        for c in use_cat_cols:
            X[c] = X[c].astype('category')
    if return_cat_cols:
        return X, cat_cols
    else:
        return X

def rate_score(df_orig, rate_coef, terms, rate_cols):
    """Score rate model
    """
    df = df_orig.copy()
    cr_score_bin_name = ['less_than_700'] + [f'{lb}_{ub}' for lb, ub in zip(range(700, 800, 20), range(720, 820, 20))]
    #Define multiplication terms:
    for term, rate_col in zip(terms, rate_cols):
        for channel in ['combined_organic', 'MKT-DM', 'Rest']:
            df[f'min_rate_{term}_{channel}'] = df[rate_col] * df[f'{channel}']
        for requested_bin in ['less_than_20000', 'more_equal_50000']:
            df[f'min_rate_{term}_{requested_bin}'] = df[rate_col] * df[f'requested_amount_{requested_bin}']
        for credit_score_bin in cr_score_bin_name:
            df[f'min_rate_{term}_{credit_score_bin}'] = df[rate_col] * df[f'credit_score_{credit_score_bin}']
    #Score logistic regression
    for term, rate_col in zip(terms, rate_cols):
        df[f'pred_rate_{term}'] = 1 / (1+np.exp(-((rate_coef[f'term_{term}'].values[1:] * \
                                      df[[rate_col, 'credit_score', 'requested_amount'] +
                                     ['combined_organic', 'MKT-DM', 'Rest'] + 
                                     ['requested_amount_less_than_20000', 'requested_amount_more_equal_50000'] +
                                     [f'credit_score_{bin}' for bin in cr_score_bin_name] +
                                     [f'min_rate_{term}_{channel}' for channel in ['combined_organic', 'MKT-DM', 'Rest']] + 
                                     [f'min_rate_{term}_{requested_bin}' for requested_bin in ['less_than_20000', 'more_equal_50000']] +
                                     [f'min_rate_{term}_{credit_score_bin}' for credit_score_bin in cr_score_bin_name]].values).sum(axis=1) + 
                                     rate_coef[f'term_{term}'].values[0])))
    df['pred_from_rate'] = df[[f'pred_rate_{term}' for term in terms]].mean(axis=1)
    df['rate_pred'] = np.clip(df['pred_from_rate'] + df['residual_pred'], 0, 1)
    return df[['id', 'rate_pred', 'pred_from_rate', 'residual_pred', 'tier_pre', 'requested_amount', 'credit_score', 'consolidated_channel_model'] + \
              ['combined_organic', 'MKT-DM', 'Rest'] + rate_cols]


def get_segment_params(param_names, segment):
    """Load mnlogit model parameters
    """
    result = []
    for item in param_names:
        if re.search(f':{segment}', item):
            result.append(item)
    return result

def term_score(df_orig, term_coef, max_rate_impute, terms, rate_cols, mask_cols=None):
    """Score multinomial logistic regression model
    """
    df = df_orig.copy()
    #Missing imputation
    for rate_col in rate_cols:
        df[rate_col+'_impute'] = np.where(df[rate_col].notnull(), df[rate_col], max_rate_impute)
    #Rate diff
    for idx, term in enumerate(range(2, 6)):
        df[f'min_rate_diff_impute_{term+1}_{term}'] = df[f'min_rate_{term+1}_impute'] - df[f'min_rate_{term}_impute']
    #Missing indicator
    for rate_col in rate_cols:
        df[rate_col+'_missing_ind'] = np.where(df[rate_col].isnull(), 1, 0)
    #Calculate logistic regression prediction
    first_term = terms[0]
    df[f'pred_term_{first_term}'] = np.exp((term_coef.loc[get_segment_params(term_coef.index, first_term)].values.reshape(-1) * \
                                            df[[f'min_rate_{first_term}_impute', f'min_rate_{first_term}_missing_ind']].values).sum(axis=1))
    for term in terms[1:]:
        coef_term = term_coef.loc[get_segment_params(term_coef.index, term)].values.reshape(-1)
        df[f'pred_term_{term}'] = np.exp((coef_term[1:] * \
                                          df[['credit_score', 'combined_organic', 'MKT-DM', 'Rest', 'requested_amount'] \
                                             + [f'min_rate_diff_impute_{temp_term + 1}_{temp_term}' for temp_term in range(2, 6)] \
                                             + [f'min_rate_{term}_impute', f'min_rate_{term}_missing_ind']].values).sum(axis=1) \
                                             + coef_term[0])
    term_prob_cols = [f'pred_term_{term}' for term in range(2, 8)]
    df = adjust_term_rate(df, rate_cols, term_prob_cols, mask_cols=mask_cols)
    return df[['id'] + term_prob_cols]

def adjust_term_rate(df, rate_cols, term_prob_cols, mask_cols=None):
    """Adjust term prediction to have 0 on missing rate terms and sum to 1
    """
    #Apply mask
    df = apply_mask_to_term_pred(df, rate_cols, term_prob_cols, mask_cols=mask_cols)
    #Adjust score to sum to 1
    df['pred_term_sum'] = df[term_prob_cols].sum(axis=1)
    for col in term_prob_cols:
        df[col] = df[col]/df['pred_term_sum']
    return df

def apply_mask_to_term_pred(df, rate_cols, term_prob_cols, mask_cols=None):
    """Apply mask to term prediction so that the prediction equals 0 for missing rate terms
    """
    if mask_cols is None:
        mask = ~df[rate_cols].isna() + 0
    else:
        mask = df[mask_cols]
    df[term_prob_cols] = df[term_prob_cols].values * mask.values
    return df            

def cal_avg(df, terms, rate_cols, term_prob_cols):
    """Score weighted average rate, weighted average maturity and weighted average loss
    """
    df['avg_rate'] = np.sum(df[rate_cols].fillna(0).values * df[term_prob_cols].values, axis=1)
    df['avg_maturity'] = np.sum(np.array(terms) * df[term_prob_cols].values, axis=1)
    return df

def cal_N_D(df):
    """Calculate N and D 
    """
    df['D'] = df['rate_pred'] * df['requested_amount'] 
    df['N'] = df['avg_rate'] * df['D']
    return df
    
def score_lightgbm(rate_model_file, term_model_file, rate_header_file, term_header_file, rate_cols, df_orig=None, data_file=None, output_file=None):
        """Score lightgbm model
        """
        if df_orig is not None:
            df = df_orig.copy()
        if data_file is not None:
            df = pd.read_csv(data_file)
        #Process data
        impute_rate_dict = {'min_rate_2': 0.1724,
                             'min_rate_3': 0.17435,
                             'min_rate_4': 0.17775,
                             'min_rate_5': 0.18983,
                             'min_rate_6': 0.15313,
                             'min_rate_7': 0.16115}
        for term, rate_col in zip(range(2, 8), rate_cols):
            df[f'min_rate_{term}_orig'] = df[rate_col]
            df[f'min_rate_{term}'] = df[rate_col].fillna(impute_rate_dict[f'min_rate_{term}'])
        rate_cols = [f'min_rate_{term}' for term in range(2, 8)]
        rate_cols_orig = [f'min_rate_{term}_orig' for term in range(2, 8)]
        term_prob_cols = [f'pred_term_{term}' for term in range(2, 8)]
        df['min_all_rates'] = df[rate_cols].min(axis=1)
        for term in range(2, 7):
            df[f'min_rate_diff_{term+1}_{term}'] = df[f'min_rate_{term+1}'] - df[f'min_rate_{term}']
        
        #Score rate model
        rate_pred = rate_score_lightgbm(df, rate_model_file, rate_header_file)
        #Score term model
        term_pred = term_score_lightgbm(df, term_model_file, term_header_file)
        #Merge score
        final_score = pd.merge(rate_pred, term_pred, on='id')
        selected_cols = ['id', 'rate_avi_ind', 'tier_pre', 'requested_amount', 'credit_score'] + ['combined_organic', 'MKT-DM', 'Rest', 'SEM'] + \
                        rate_cols + rate_cols_orig 
        final_score = pd.merge(final_score, df[selected_cols], on='id')
        #Adjust term score to sum to 1
        final_score = adjust_term_rate(final_score, rate_cols_orig, term_prob_cols)
        #Calculate weighted average column
        final_score = cal_avg(final_score, range(2, 8), rate_cols, term_prob_cols)
        final_score = cal_N_D(final_score)
        
        if output_file is not None:
            final_score.to_csv(output_file, index=None)
        return final_score
    
def term_score_lightgbm(df, term_model_file, term_header_file):
    """Score term model using lightgbm model
    """
    term_header = Header(term_header_file)
    term_model = read_model(term_model_file)
    term_data = clean_data(df, term_header)
    term_pred = pd.DataFrame(term_model.predict(term_data))
    term_pred.columns = [f'pred_term_{term}' for term in range(2, 8)]
    term_pred['id'] = df['id']
    return term_pred

def rate_score_lightgbm(df, rate_model_file, rate_header_file):
    """Score rate model using lightgbm model
    """
    rate_header = Header(rate_header_file)
    rate_model = read_model(rate_model_file)
    rate_data = clean_data(df, rate_header)
    rate_pred = pd.DataFrame(rate_model.predict(rate_data))
    rate_pred.columns = ['rate_pred']
    rate_pred['id'] = df['id']
    return rate_pred

def read_model(model_file):
    """Return model from model_file
    """
    mdl = lgb.Booster(model_file=model_file)
    return mdl

def clean_data(df, header):
    """Clean data for lightgbm scoring with headr file
    """
    X = df[header.num_cols + header.cat_cols]
    X = object2cat(X, use_cat_cols=header.cat_cols)
    return X

def clean_str(str_col):
    """Return lower case of str_col and replace ' ' and '-' with '_'
    """
    str_col = str_col.str.lower()
    str_col = str_col.str.replace(" ",'_')
    str_col = str_col.str.replace("-",'_')
    return str_col

def flat_rate(df, terms, term_col='initial_term', rate_col='interest_rate'):
    """Create columns to map interest_rate and initial term to individual term columns
    """
    rate_map_cols = [f'rate_map_{term}' for term in terms]
    for term, col in zip(terms, rate_map_cols):
        df[col] = np.where(df[term_col] == term, df[rate_col], 0)
    return df

def cut_cat_var(df, cat_var, cat_var_value_list):
    """For a given cat_var in a DataFrame df, keep values in cat_var_value_list and group other values in 'Rest'
    """
    df[cat_var] = np.where((df[cat_var].isin(cat_var_vSalue_list)) | (df[cat_var].isnull()), df[cat_var], 'Rest')
    return df

def add_signed_ind(df, model_target='signed_ind', term_var='initial_term', terms=None, filterdays=30, remove_mix_labels=True):
    """Set the signed indicator by terms and filter out the ones with signed timewindow > filterdays
    Also change target variable accordingly by timewindow
    """
    df['date_start'] = pd.to_datetime(df['date_start'])
    df['date_signed'] = pd.to_datetime(df['date_signed'])
    if terms is None: 
        #Infer terms list from "initial_term" variable
        terms = df['initial_term'].unique() #take distinct term values as a numpy array
        terms = sorted(terms[~np.isnan(terms)].astype(int))
    for term in terms:
        df[model_target + '_{0}'.format(term)] = np.where((df[model_target] == 1) & (df[term_var] == term) & 
                                                          (df['date_signed'] - df['date_start'] <= pd.Timedelta(filterdays,'D')), 1, 0)
    df[model_target] = np.where(df['date_signed'] - df['date_start'] > pd.Timedelta(filterdays,'D'), 0, df[model_target])
    if remove_mix_labels:
        df = df[~(df['date_signed'] - df['date_start'] > pd.Timedelta(filterdays,'D'))]
    return df

def query_snowflake(qry):
    """Return the pandas DataFrame from input SQL qry with connection to snowflake
    """
    start = time.time()
    sc = snowflake.connector.connect(user='sdm_ro', password='none', port=1444, host='localhost', account='sofi', protocol='http')
    df = pd.read_sql(qry, sc)
    sc.close()
    df.columns = df.columns.str.lower()
    end = time.time()
    print(f'snowflake in {end - start}[s]')
    return df